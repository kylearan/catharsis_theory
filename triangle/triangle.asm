    echo ""
    echo "------------------------------------------------------------"
    echo "TRIANGLE:"

; =====================================================================
; Constants
; =====================================================================
STRIP_X_OFFSET      = 64
STRIP_Y_OFFSET      = 125
STRIP_NORMAL_TILT   = 95

STRIP_BASECOL0      = $20
STRIP_BASECOL1      = $90
STRIP_BASECOL2      = $30

BACKGROUND_BASECOL  = $40


; =====================================================================
; Subroutines
; =====================================================================
TriangleStripeKernel:   SUBROUTINE
    sta WSYNC
    sta 0,y                     ; 4
    ; @4
    ; Update PF
    txa                         ; 2
    and #%00001111              ; 2
    tay                         ; 2
    lda tr_pf_buffer,y          ; 4
    and #%11000000              ; 2
    sta PF0                     ; 3
    lda tr_pf_buffer,y          ; 4
    sta PF1                     ; 3
    sta PF2                     ; 3
    ; Set color
    lda tr_gradient,y           ; 4
    sta COLUPF                  ; 3

    ; @36
    ; Check for next line HMOVE
    ldy #CXCLR                  ; 2
    lda tr_currentTilt          ; 3
    clc                         ; 2
    adc #STRIP_NORMAL_TILT      ; 2
    sta tr_currentTilt          ; 3
    bcc .noHmove                ; 2/3
    ldy #HMOVE                  ; 2
.noHmove:
    ; @53 max
    inx                         ; 2
    cpx tr_current_end          ; 3
    bne TriangleStripeKernel    ; 2/3

    echo "[M]", TriangleStripeKernel, "to", *, ": TriangleStripeKernel"

    ; @60 max
    rts                         ; 6


; ---------------------------------------------------------------------
; Position P/M for a stripe.
; IN:
;   a   stripe width
;   y   stripe center
;   x   Index of player object
; OUT:
;   a   half of stripe width
; ---------------------------------------------------------------------

TrianglePosition:   SUBROUTINE
    pha
    sty tr_tmp2
    ; Compute half width
    ; Counter goes from 0..31, but width from 8..39
    clc
    adc #8
    pha                         ; needed later
    lsr
    sta tr_tmp
    ; Compute GRP
    lsr
    lsr
    tay
    lda TriangleGrp-1,y
    sta GRP0,x
    ; Position P
    lda tr_tmp2
    sec
    sbc tr_tmp
    adc tr_aggroOffset
    FINEPOS
    ; x is +1 now
    ; Position M
    pla
    lsr
    lda tr_tmp2
    ; use carry from lsr above
    adc tr_tmp
    adc tr_aggroOffset
    ; -8 for missile, +2 for missile relative to player placement
    ; carry is clear now
    sbc #5
    inx
    FINEPOS
    pla
    lsr
    rts


; ---------------------------------------------------------------------
; Get sin value for index in [0..63] and value in [0..30]
; IN:
;   a   index
; OUT:
;   a   sin value
; ---------------------------------------------------------------------
GetSin  SUBROUTINE
    and #63
    ; Mirror?
    cmp #32
    bcc .noMirror
    eor #31
.noMirror:
    and #31
    tay
    lda TriangleSin,y
    rts


; =====================================================================
; Data
; =====================================================================
; TriangleGrp is in tunnel.asm now

; Mode change script timestamps
RlModeChangeTimestamps:
    ; First relief
    dc.b 180, 185
    dc.b 220, 225
    dc.b 240, 250
    dc.b 80, 90
    dc.b 130, 180
    dc.b 210
    ; Second relief
    dc.b 90, 110
    dc.b 150, 170
    dc.b 1, 6
    dc.b 11, 16
    dc.b 30, 100
    dc.b 150
    ; Last relief
    dc.b 60, 85
    dc.b 115, 130
    dc.b 180, 240
    dc.b 50
    ; overlap
    ;dc.b 0
TriangleSin:
    dc.b 0, 0, 0, 1, 1, 2, 3, 3
    dc.b 4, 5, 7, 8, 9, 11, 12, 14
    dc.b 15, 16, 18, 19, 21, 22, 23, 25
    dc.b 26, 27, 27, 28, 29, 29, 30, 30

TrianglePf:
    dc.b %10000000
    dc.b %01000000
    dc.b %00100000
    dc.b %00010000
    dc.b %00001000
    dc.b %00000100
    dc.b %00000010
    dc.b %00000001

    echo "[M]", TrianglePf, "to", *, ": TrianglePf"


; =====================================================================
; VBlank
; =====================================================================

; ---------------------------------------------------------------------
; VBlank main
; ---------------------------------------------------------------------
TriangleVBlank  SUBROUTINE
    ; Place objects
    ; numFrame is in a already
    ldx aggression
    beq .noAggression
    asl
.noAggression:
    pha
    ; Before positioning, check for super aggro
    cpx #2
    bne .noSuperAggro
    jsr Rnd
    and #7
    sbc #3
    tax
.noSuperAggro:
    stx tr_aggroOffset
    pla
    pha
    jsr GetSin
    ldx #0
    ldy #(160 - STRIP_X_OFFSET)
    jsr TrianglePosition
    adc #STRIP_BASECOL0
    sta tr_col0
    sta COLUP0

    ; carry is clear
    pla
    pha
    adc #20
    jsr GetSin
    ldy #(STRIP_X_OFFSET - 3)
    ldx #1
    stx CTRLPF
    jsr TrianglePosition
    adc #STRIP_BASECOL1
    sta COLUP1

    sta WSYNC
    sta HMOVE

    ; Compute horizontal strip
    pla
    ; carry is clear
    adc #40
    jsr GetSin
    clc
    adc #14
    tax
    lsr
    pha
    eor #$ff
    sec
    adc #STRIP_Y_OFFSET
    sec
    sbc tr_aggroOffset
    sta tr_current_end

    txa
    clc
    adc tr_current_end
    sta tr_horizontal_end
    pla
    lsr
    adc #STRIP_BASECOL2
    sta tr_col2

    lda #HM_LEFT_1              ; 2
    sta HMP0                    ; 3
    sta HMM0                    ; 3
    ; OR 2 for ENAM
    lda #HM_RIGHT_1 | 2         ; 2
    sta HMP1                    ; 3
    sta HMM1                    ; 3
    sta ENAM0
    sta ENAM1
    sta tr_currentTilt

PrepareBGBuffers    SUBROUTINE
    ; Prepare PF and gradient buffers
    ldx #15
.prepareBuffers:
    ; scrolling
    txa
    sec
    sbc numFrame
    ldy aggression
    beq .noAggression
    sbc numFrame
.noAggression:
    adc #12                     ; looks best (least worst...)
    jsr GetSin
    and #15
    tay
    cmp #7
    bcc .noInvert
    eor #15
.noInvert:
    asl
    ; PF color
    sbc tr_bg_intensity
    bcs .noUnderflow
    lda #0
    beq .storeColor             ; unconditional
.noUnderflow:
    cmp #$10
    bne .notWhite
    lda #$0e
    bne .storeColor             ; unconditional
.notWhite:
    ; carry is clear
    adc #BACKGROUND_BASECOL
.storeColor:
    sta tr_gradient,x
    ; PF buffer
    tya
    lsr
    tay
    lda TrianglePf,y
    sta tr_pf_buffer,x
    dex
    bpl .prepareBuffers

    ; Control
    lda #%00110111          ; Quad sized player, missile size 8
    sta NUSIZ0
    sta NUSIZ1

    ; PF Intensity drain
    inc tr_bg_intensity

    jmp tt_Player


; =====================================================================
; Kernel
; =====================================================================

TriangleKernel  SUBROUTINE
    ; numFrame is in a
    and #%00011111
    bne .noIntensityReset
    sta tr_bg_intensity
.noIntensityReset:

    ldx #0                      ; 2
    ldy #CXCLR                  ; 2
    jsr TriangleStripeKernel

    ; @66 max
TriangleHorizontalLoop: SUBROUTINE
    sta WSYNC
    sta 0,y                     ; 4

    ; @4
    lda tr_col2                 ; 3
    sta COLUPF                  ; 3
    sta COLUP0                  ; 3
    lda #%11000000              ; 2
    sta PF0                     ; 3
    lda #$ff                    ; 2
    sta PF1                     ; 3
    sta PF2                     ; 3

    ; @26
    ; Check for next line HMOVE
    ldy #CXCLR                  ; 2
    lda tr_currentTilt          ; 3
    clc                         ; 2
    adc #STRIP_NORMAL_TILT      ; 2
    sta tr_currentTilt          ; 3
    bcc .noHmove                ; 2/3
    ldy #HMOVE                  ; 2
.noHmove:
    ; @43 max
    inx                         ; 2
    cpx tr_horizontal_end       ; 3
    bne TriangleHorizontalLoop  ; 2/3

    echo "[M]", TriangleHorizontalLoop, "to", *, ": TriangleHorizontalLoop"

    ; @50 max
    lda #200
    sta tr_current_end
    lda tr_col0
    sta COLUP0
    jsr TriangleStripeKernel
    ; Fall through to CleanUpGfxWithBg

; ---------------------------------------------------------------------
; Clean up GFX registers
; Is now in triangle.asm
; ---------------------------------------------------------------------
CleanUpGfxWithBg    SUBROUTINE
    lda #0
    sta COLUBK
CleanUpGfx  SUBROUTINE
    lda #0
    sta COLUP0
    sta COLUP1
    sta COLUPF
    sta GRP0
    sta GRP1
    sta ENAM0
    sta ENAM1
    sta ENABL
    sta PF0
    sta PF1
    sta PF2
    sta REFP0
    sta RESMP0
    sta RESMP1

TriangleOverscan:
    rts

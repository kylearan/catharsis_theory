    ORG     START_VAR_PARTS
; Gets subtracted from the PF color
tr_bg_intensity         ds 1

    echo "Triangle permanent RAM left: ",($100 - *)

tr_tmp                  ds 1
tr_tmp2                 ds 1
tr_currentTilt          ds 1
tr_aggroOffset          ds 1
; Stripe colors (PM0, PM1, horizontal)
tr_col0                 ds 1
tr_col2                 ds 1
; Start and end of horizontal stripe
tr_horizontal_end       ds 1
tr_current_end          ds 1
; PF frame buffers
tr_pf_buffer            ds 16
; Color gradient buffer
tr_gradient             ds 16

    echo "Triangle stack RAM left: ",($100 - *)

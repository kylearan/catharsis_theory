	MAC VERBOSE_ALIGN
		echo "[*] VERBOSE_ALIGN:", (256 - (* & $ff)), "byte cave at", *
		align 256
	ENDM

	MAC FINEPOS
		brk
		nop
	ENDM

	; NOCROSS <size_of_data>
	MAC NOCROSS
		IF ( (256 - (* & $ff)) < {1} )
			ECHO "[*] NOCROSS: ", (256 - (* & $ff)), "byte cave at", *
			ALIGN 256
		ENDIF
	ENDM

  MAC SLEEP
    IF {1} = 1
      ECHO "ERROR: SLEEP 1 not allowed !"
      END
    ENDIF
    IF {1} & 1
      nop $00
      REPEAT ({1}-3)/2
        nop
      REPEND
    ELSE
      REPEAT ({1})/2
        nop
      REPEND
    ENDIF
  ENDM

	MAC SL2
		nop
	ENDM
	MAC SL3
		SLEEP 3
	ENDM
	MAC SL4
		nop
		nop
	ENDM
	MAC SL5
		dec $2d
	ENDM
	MAC SL6
		SLEEP 6
	ENDM
	MAC SL7
		pha
		pla
	ENDM
	MAC SAFESL7
		SL5
		nop
	ENDM
	MAC SL8
		SL6
		nop
	ENDM
	MAC SL9
		SL7
		nop
	ENDM
	MAC SL10
		SL5
		SL5
	ENDM
	MAC SL11
		SL7
		SL4
	ENDM
	MAC SAFESL11
		SL6
		SL5
	ENDM
	MAC SL12
		jsr Return
	ENDM
	MAC SAFESL12
		SL5
		SL5
		nop
	ENDM


  MAC CHECKPAGE
    IF >. != >{1}
      ECHO ""
      ECHO "ERROR: different pages! (", {1}, ",", ., ")"
      ECHO ""
      ERR
    ENDIF
  ENDM

;	MAC CROSSCHECK
;		IF (*>>8) > ({1}>>8)
;			echo "***", {1}, "is on different page than ", *, "!"
;		ENDIF
;	ENDM

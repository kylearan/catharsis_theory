; Glyph constants

    ORG 0
LN_A    ds 5
LN_C    ds 5
LN_E    ds 5
LN_F    ds 5
LN_G    ds 5
LN_H    ds 5
LN_I    ds 5
LN_L    ds 5
LN_M    ds 5
LN_N    ds 5
LN_O    ds 5
LN_P    ds 5
LN_R    ds 5
LN_S    ds 5
LN_T    ds 5
LN_U    ds 5
LN_W    ds 5
LN_Y    ds 5

; Vars

    ORG     START_VAR_PARTS
ln_seed                 ds 1
ln_seed_2               ds 1
ln_lightningFrame       ds 1    ; counts up from 0 after last flash
ln_nextLightning        ds 1    ; frame number of next lightning
ln_backColor            ds 1

    echo "Lightning permanent RAM left: ",($100 - *)

ln_tmp                  ds 1
ln_curMainDir           ds 1    ; Current dir of main line. $01: left 1, $ff: right 1
ln_branchState          ds 1    ; 64: single branch, 128: double branch, 0: recall
ln_branchDir            ds 1    ; Current dir of branch ($01 or $ff). Second branch will be inverted.
ln_PF1                  ds 5
ln_PF2                  ds 5
ln_PF3                  ds 5
ln_PF4                  ds 5
ln_PF5                  ds 5
ln_glyphIn              ds 5

    echo "Lightning stack RAM left: ",($100 - *)

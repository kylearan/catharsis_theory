    echo ""
    echo "------------------------------------------------------------"
    echo "LIGHTNING:"

; =====================================================================
; Data
; =====================================================================

; Script data: Length of part hi-byte
DUR_SHORT   = 1
DUR_MEDIUM  = 0
DUR_LONG    = 2
DUR_AUTO    = 0

; LightningGlyphs is now in relief.asm

LightningWords:
    dc.b LN_H, LN_U, LN_R, LN_T, 256-9
    dc.b LN_S, LN_H, LN_A, LN_M, LN_E, 256-7
    dc.b LN_P, LN_A, LN_I, LN_N, 256-12
    dc.b LN_F, LN_E, LN_A, LN_R, 256-10

    dc.b LN_S, LN_P, LN_I, LN_T, LN_E, 256-9
    dc.b LN_R, LN_A, LN_G, LN_E, 256-10
    dc.b LN_S, LN_C, LN_O, LN_R, LN_N, 256-8
    dc.b LN_M, LN_A, LN_L, LN_I, LN_C, LN_E, 256-6

    dc.b LN_H, LN_A, LN_T, LN_E, 256-10
    dc.b LN_R, LN_A, LN_N, LN_C, LN_O, LN_R, 256-4
    dc.b LN_F, LN_U, LN_R, LN_Y, 256-9
    dc.b LN_W, LN_R, LN_A, LN_T, LN_H, 256-6

; Good-looking seeds
LightningSeeds:
    dc.b 5, 15, 38, 47, 53, 58, 148
    ; Overlap
    ;dc.b 180
PistolTimes:
    dc.b 180, 193, 220, 233, 246


; =====================================================================
; VBlank
; =====================================================================

; ---------------------------------------------------------------------
; Lightning init
; ---------------------------------------------------------------------
LightningVBlank SUBROUTINE
    ; numFrame is in a already
    bne .noInit
    ; Init vars
    sta ln_backColor
    lda #64
    sta ln_lightningFrame
    asl
    sta ln_nextLightning
.noInit:

    ; check for new flash
    ldx ln_lightningFrame
    inx
    cpx ln_nextLightning
    bne .noNewFlash
    ; new Flash: duration = 20+rnd(63)
    lda ln_seed_2
    jsr Rnd
    sta ln_seed_2
    and #63
    adc #20
    sta ln_nextLightning
    lda #$11
    sta ln_backColor
    ; new form
    dec ln_lightningType
    ldx #0
.noNewFlash:
    stx ln_lightningFrame

    ; Background: Flash or black
    ldy ln_backColor
    dey
    bpl .noUnderflow
    ldy #0
.noUnderflow:
    sty ln_backColor
    sty COLUBK

    ; Lightning color
    txa                     ; ln_lightningFrame
    sbc #10
    bcs .noUnderflow2
    lda #0
.noUnderflow2:
    lsr
    sta ln_tmp
    eor #$0f
    cmp #$10
    bcc .noUnderflow3
    lda #0
.noUnderflow3:
    sta COLUPF
    lda #$0a
    sec
    sbc ln_tmp
    bcs .noZero2
    lda #0
    .dc $2c                 ; bit xxxx to skip ora
.noZero2:
    ora #$d0
    sta COLUP0
    sta COLUP1

    ; Position of lightning is 60 + ln_seed_2%31
    lda ln_seed_2
    and #31
    clc
    adc #60
    tay                     ; save for P0 and P1
    ; Position ball
    ldx #4
    stx CTRLPF              ; Ball size 1, Prio above
    FINEPOS
    ; Position P0 and P1 (2 less than ball)
    dey
    dey
    tya
    ldx #0
    stx NUSIZ0
    stx NUSIZ1
    stx ln_branchState      ; Start state 0: recall, missiles disabled
    FINEPOS
    ; x is now 1
    stx ln_curMainDir
    tya
    FINEPOS
    ; x is now 2
    sta WSYNC
    sta HMOVE
    ; Position (hide) M
    stx RESMP0
    stx RESMP1

    ; Prepare parameters
    lda ln_lightningType
    and #7
    tax
    lda LightningSeeds,x
    sta ln_seed

    ; Fall through to EnsureNormalMusic
    include "music/music_player.asm"
    rts


; =====================================================================
; Kernel
; =====================================================================

; ---------------------------------------------------------------------
; Construct text
; Will be called by kernel during frame #0.
; Placed before kernel so a conditional branch can reach it.
; ---------------------------------------------------------------------
LightningPrepareText    SUBROUTINE
    ; Prepare text
    ; clear buffer
    ldx #24 ; could be lower, as parts will be scrolled out
.clearLoop:
    sta ln_PF1,x
    dex
    bpl .clearLoop

    ; ---------- Fetch and construct word ----------
.fetchLetterLoop:
    ldx ln_currentGlyph
    ; Get next letter
    ldy LightningWords,x
    bmi .endFetch
    ; Copy glyph inverted to staging area
    ldx #4                  ; row
.copyGlyph:
    lda LightningGlyphs,y
    and #%11111000          ; mask out script duration data
    sta ln_glyphIn,x
    iny
    dex
    bpl .copyGlyph

    ; Scroll in letter
.scrollInLetterLoop:
    jsr LightningScroll
    bvs .scrollInLetterLoop

    inc ln_currentGlyph
    bne .fetchLetterLoop    ; unconditional

.endFetch:
    ; store next glyph for next iteration
    inc ln_currentGlyph
    ; Center word: 256-y is the number of scrolls to center
.centerLoop:
    jsr LightningScroll
    iny
    bne .centerLoop

LightningOverscan:
    rts


; ---------------------------------------------------------------------
; Kernel entry point
; ---------------------------------------------------------------------
LightningKernel SUBROUTINE
    ; First frame: construct text and init vars instead of displaying anything
    beq LightningPrepareText

; ---------------------------------------------------------------------
; Lightning
; ---------------------------------------------------------------------
    ; Switch on objects
    ldx #2
    stx ENABL
    lda #$e0
    sta GRP0
    ; Release missiles. Now they are 3 pixels right of the lightning.
    sta RESMP0              ; bit #2 is 0
    sta RESMP1

    ; Loop
    ; x is not used, so use it for constant #2
.loop:
    ; ---------- main line ----------
    ; Get rnd number
    lda ln_seed
    beq .doEor
    asl
    beq .noEor
    bcc .noEor
.doEor
    eor #$1d
.noEor
    sta ln_seed
    ; Check for direction change
    and #15
    bne .noDirChange
    ; Switch direction
    lda ln_curMainDir
    eor #$fe
    sta ln_curMainDir
.noDirChange:

    ; Apply wiggle to main dir
    ldy ln_curMainDir
    bit ln_seed
    bmi .noWiggle           ; bit #7: wiggle flag
    bvs .doAdd              ; bit #6: add or sub 1
    dey
    dc.b $24                ; BIT to skip next instruction
.doAdd:
    iny
.noWiggle:

    ; Do movement and loop
    ; y = direction ($02, $01, $00, $ff, $fe)
    tya
    asl
    asl
    asl
    asl
    sta HMP0
    sta HMP1
    sta HMBL

    ; ---------- branches ----------
    ; Multiplex state
    lda ln_branchState
    bne .noRecall
    ; Recall: do LEFT_3, reset state and set new branch direction for next iteration
    lda #$30
    sta HMM0
    sta HMM1
Bit6Set:
    lda #64     ; i.e. bit #6 is set, also used elsewhere to set v
    sta ln_branchState
    ; Direction is opposite of main branch
    lda ln_curMainDir
    eor #$fe
    sta ln_branchDir
    bne .afterBranch        ; unconditional

.noRecall:
    ; Apply wiggle to first branch
    ldy ln_branchDir
    bit ln_seed
    bvs .noBranchWiggle     ; bit #6: wiggle flag
    bmi .doBranchAdd        ; bit #7: add or sub 1
    dey
    dc.b $24                ; BIT to skip next instruction
.doBranchAdd:
    iny
.noBranchWiggle:
    ; Set HMM0, same for both states
    ; y = direction ($02, $01, $00, $ff, $fe)
    tya
    asl
    asl
    asl
    asl
    sta HMM0
    ; State either single or double
    bit ln_branchState
    bmi .doubleBranch
    ; Single branch
    sta HMM1
    bpl .checkStateChange   ; unconditional

    ; Double branch
.doubleBranch:
    ; Apply wiggle to second branch
    ldy ln_branchDir
    bit ln_seed
    bmi .noBranchWiggle2    ; bit #7: wiggle flag
    bvs .doBranchAdd2       ; bit #6: add or sub 1
    dey
    dc.b $24                ; BIT to skip next instruction
.doBranchAdd2:
    iny
.noBranchWiggle2:
    ; invert direction
    tya
    eor #$ff
    adc #$01
    asl
    asl
    asl
    asl
    sta HMM1
    ; Switch off one branch randomly
    lda ln_seed
    and #%00011000
    bne .noSwitchOff
    stx RESMP0
.noSwitchOff:

.checkStateChange:
    ; Check for state change of branch
    lda ln_seed
    and #%00001110
    bne .noStateChange
    ; advance state
    asl ln_branchState
    bne .noStateChange
    ; Initiate recall
    sta ENAM0
    sta ENAM1
    stx RESMP0
    stx RESMP1
.noStateChange:

.afterBranch:

    ; ---------- move, re-enable and loop ----------
    sta WSYNC
    sta HMOVE
    ; Only re-enable missiles of not in recall state
    lda ln_branchState
    beq .noReEnable
    sta RESMP0              ; bit #2 is 0
    sta RESMP1
    stx ENAM0
    stx ENAM1
.noReEnable:
    lda INTIM
    cmp #4
    beq .endLoop
    jmp .loop
.endLoop:
    tax                     ; save 4 for later
    ; Switch off objects
    jsr CleanUpGfx

; ---------------------------------------------------------------------
; Text
; ---------------------------------------------------------------------
LightningKernelText SUBROUTINE
    ; Set up text and Cosmic Ark text overlay
    jsr GetFlashCol
    tay
    sta COLUPF
    ; Objects
    lda ln_backColor
    sta COLUP0
    sta COLUP1
    lda #%00000011
    sta GRP0
    sta GRP1
    lda #0
    sta CTRLPF
    jsr InitCosmicArk
.posLoop:
    dey
    bpl .posLoop
    sta RESP0
    sta RESP1

    ; Display text
    ; x is 4 already from INTIM above
.rowLoop:
    ldy #6
.pixelLoop:
    sta WSYNC               ; 3
    lda #0                  ; 2
    sta PF0                 ; 3
    lda ln_PF1,x            ; 4
    sta PF1                 ; 3
    lda ln_PF2,x            ; 4
    sta PF2                 ; 3

    SL12                    ; 12

    lda ln_PF3,x            ; 4
    sta PF0                 ; 3
    lda ln_PF4,x            ; 4
    sta PF1                 ; 3
    lda ln_PF5,x            ; 4
    sta PF2                 ; 3

    dey                     ; 2
    bne .pixelLoop          ; 2/3
    dex                     ; 2
    bpl .rowLoop            ; 2/3

    ; clear text
    sta HMOVE               ; to clear Cosmic Ark effect
    jmp CleanUpGfx


; ---------------------------------------------------------------------
; Scroll word one pixel left.
; Trashes x, but leaves y alone.
; ---------------------------------------------------------------------
LightningScroll SUBROUTINE
    ldx #4                  ; row
    clv                     ; Loop condition: use v to keep track if letter is completely scrolled in
    ; scroll in letter
.scrollInPixel:
    rol ln_glyphIn,x
    bcc .bitClear
    bit Bit6Set+1           ; Set v to indicate a set bit was seen
.bitClear:
    ; scroll
    ror ln_PF5,x
    rol ln_PF4,x
    ror ln_PF3,x
    lda ln_PF3,x            ; crippled PF2 is special case: transfer bit #3 to c
    clc
    and #%00001000
    beq .bitNotSet
    sec
.bitNotSet:
    ror ln_PF2,x
    rol ln_PF1,x
    ; loop
    dex
    bpl .scrollInPixel

    rts

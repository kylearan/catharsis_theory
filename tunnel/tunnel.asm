    echo ""
    echo "------------------------------------------------------------"
    echo "TUNNEL:"

; =====================================================================
; Data
; =====================================================================

; Combines angles (bits 0..4) and textures (bits 6..7)
TunnelAngles:
	dc.b $51, $12, $93, $93, $94, $95, $d6, $57, $58, $59, $1b, $1c, $1d, $1e, $00, $41
	dc.b $10, $91, $92, $13, $14, $15, $d6, $d7, $58, $59, $1a, $1b, $1d, $1e, $1f, $01
	dc.b $90, $90, $11, $12, $53, $54, $55, $d6, $d7, $58, $5a, $5b, $1c, $1e, $1f, $01
	dc.b $8f, $10, $10, $51, $d2, $53, $54, $15, $96, $d8, $59, $5a, $5c, $1d, $1f, $81
	dc.b $0e, $0f, $50, $d0, $d1, $92, $13, $14, $16, $97, $98, $5a, $5b, $5d, $9f, $81
	dc.b $0d, $0e, $cf, $cf, $d0, $91, $92, $13, $15, $16, $97, $d9, $5b, $dd, $9e, $00
	dc.b $0c, $cd, $ce, $ce, $8f, $90, $91, $92, $14, $15, $17, $98, $da, $dc, $5e, $00
	dc.b $cb, $cc, $cd, $cd, $0e, $8f, $90, $d1, $d2, $14, $16, $17, $d9, $5b, $5e, $40
	dc.b $ca, $cb, $cb, $4c, $0d, $0e, $8f, $d0, $d1, $93, $14, $16, $58, $5b, $5d, $c0
	dc.b $89, $ca, $4a, $4b, $0c, $0c, $0d, $8e, $90, $91, $93, $15, $57, $5a, $dc, $9f
	dc.b $88, $08, $49, $49, $4a, $0b, $0c, $0d, $8e, $90, $91, $d3, $56, $d8, $9b, $9f
	dc.b $07, $07, $48, $48, $49, $09, $0a, $0b, $0c, $8e, $8f, $d1, $d4, $d7, $9a, $9e
	dc.b $05, $06, $06, $47, $47, $48, $48, $09, $0a, $4b, $cd, $cf, $d1, $94, $98, $1d
	dc.b $04, $04, $05, $05, $45, $46, $46, $47, $48, $49, $4a, $cc, $8e, $91, $16, $1b
	dc.b $03, $03, $03, $03, $84, $04, $44, $45, $45, $46, $07, $08, $8a, $0d, $11, $18
	dc.b $41, $01, $02, $82, $02, $82, $42, $42, $43, $43, $04, $04, $05, $07, $0a, $51

TunnelDistances:
	dc.b $70, $78, $78, $80, $80, $80, $88, $88, $90, $90, $98, $98, $98, $a0, $a0, $a0
	dc.b $78, $78, $80, $80, $88, $88, $90, $90, $98, $98, $a0, $a0, $a8, $a8, $a8, $a8
	dc.b $78, $80, $80, $88, $88, $90, $98, $98, $a0, $a0, $a8, $b0, $b0, $b0, $b8, $b8
	dc.b $80, $80, $88, $88, $90, $98, $a0, $a0, $a8, $b0, $b0, $b8, $c0, $c0, $c0, $c8
	dc.b $80, $88, $88, $90, $98, $a0, $a0, $a8, $b0, $b8, $c0, $c8, $c8, $d0, $d0, $d8
	dc.b $80, $88, $90, $98, $a0, $a8, $b0, $b8, $c0, $c8, $d0, $d0, $d8, $e0, $e8, $e8
	dc.b $88, $90, $98, $a0, $a0, $b0, $b8, $c0, $c8, $d0, $d8, $e8, $f0, $f8, $f8, $00
	dc.b $88, $90, $98, $a0, $a8, $b8, $c0, $c8, $d8, $e0, $f0, $f8, $00, $10, $18, $18
	dc.b $90, $98, $a0, $a8, $b0, $c0, $c8, $d8, $e0, $f0, $00, $10, $20, $28, $38, $40
	dc.b $90, $98, $a0, $b0, $b8, $c8, $d0, $e0, $f0, $00, $18, $28, $40, $50, $60, $68
	dc.b $98, $a0, $a8, $b0, $c0, $d0, $d8, $f0, $00, $18, $30, $48, $60, $80, $98, $a8
	dc.b $98, $a0, $b0, $b8, $c8, $d0, $e8, $f8, $10, $28, $48, $68, $90, $b8, $d8, $f8
	dc.b $98, $a8, $b0, $c0, $c8, $d8, $f0, $00, $20, $40, $60, $90, $c8, $00, $40, $70
	dc.b $a0, $a8, $b0, $c0, $d0, $e0, $f8, $10, $28, $50, $80, $b8, $00, $58, $c8, $28
	dc.b $a0, $a8, $b8, $c0, $d0, $e8, $f8, $18, $38, $60, $98, $d8, $40, $c8, $88, $78
	dc.b $a0, $a8, $b8, $c8, $d8, $e8, $00, $18, $40, $68, $a8, $f8, $70, $28, $78, $10

        echo "[M]", TunnelAngles, "to", *, ": TunnelAngles and TunnelDistances"


; =====================================================================
; Subroutines
; =====================================================================

; ---------------------------------------------------------------------
; Compute some lines of the framebuffer
; IN:
;   tn_y    start y
;   a   y index to stop at
; ---------------------------------------------------------------------

; ---------------------------------------------------------------------
; Left top, Right right, even frames:
; left_1 top    -> right_2 top
; left_2 top    -> right_2 bottom
; ---------------------------------------------------------------------

ShotgunSfx:
  dc.b 12, 12, 12, 10, 8, 6, 4, 2
  ; overlap
  ; dc.b 0
SHOTGUN_LEN = * - ShotgunSfx + 1
TunnelMul16
    dc.b 0, 16, 32, 48, 64, 80, 96, 112

    echo "[M]", TunnelMul16, "to", *, ": TunnelMul16"


; =====================================================================
; Overscan
; =====================================================================

TunnelOverscan  SUBROUTINE
    lda numFrame
    bne .noInit
    ; First frame: Init
    lda numPart
    lsr
    lda #64
    bcs .odd
    asl
.odd:
    sta tn_tex_mask
    lda #5
    sta tn_y
.noInit:

    ; Init GFX and place objects
    ldx #255
    stx PF0
    inx
    lda #47
    FINEPOS
    ; x is 1 now
    stx CTRLPF
    lda #79
    FINEPOS
    sta WSYNC
    sta HMOVE

    ; Create next lines of tunnel
    inx                         ; x is 3 now
    bne TunnelCompute           ; unconditional


; =====================================================================
; VBlank
; =====================================================================

TunnelVBlank    SUBROUTINE
    ; Complete tunnel
    ldx #255
    ; fall through to TunnelCompute

; ---------------------------------------------------------------------
; Call TunnelComputeTop or Bottom depending on odd/even frame
; IN:
;   x   y index to stop at
; ---------------------------------------------------------------------

TunnelCompute   SUBROUTINE
    stx tn_compute_upto
    lda tn_numFrame
    lsr
    tsx
    stx tn_stack
    ldx tn_y
    bcc TunnelComputeBottom     ; odd/even
.odd:
    ; fall through to TunnelComputeTop

TunnelComputeTop    SUBROUTINE
TunnelComputeTopLoop:
.loopY:
    ; calc base index into angles/distances, i.e. y*16 for later use as y*16+x
    ldy TunnelMul16,x
    ; for x = 0 to 7. Use 248 to 255 instead so inx; bne can be used
    ldx #248
.loopX:
    ; ---------- Left_1 ----------
    ; u = ((angles(x, y) + add_rotate)/PRECISION_FAC)%TEX_SIZE
    lda TunnelAngles,y          ; 4
    adc tn_add_rotate           ; 3
    asr #%00011110              ; 2, %TEX_SIZE, /2 for PRECISION_FAC
    sta tn_u                    ; 3
    ; v = ((distances(x, y) + add_dist)/PRECISION_FAC)%TEX_SIZE
    lda TunnelDistances,y       ; 4, is *8 already
    adc tn_add_distance         ; 3, must be *8 already
    ; /PRECISION_FAC;%TEX_SIZE;*2 can be collapsed into and #%11110000
    and #%11110000              ; 2
    ; get pixel from texture, i.e. a = texture(u, v) = texture(8*v+u)
    ora tn_u                    ; 3
    txs                         ; 2, x=tn_x is needed later again
    tax                         ; 2
    lda TunnelAngles,x          ; 4
    and tn_tex_mask             ; 3

    ; store in top left quadrant
    cmp #1                      ; 2
    rol tn_cur_left_1           ; 5

    ; store rotated in top right quadrant
    tsx                         ; 2, get tn_x as index
    cmp #1                      ; 2
    ror tn_right_2+8,x          ; 6, +8 to adjust for loop counter

    ; ---------- Left_2 ----------
    ; u = ((angles(x+8, y) + add_rotate)/PRECISION_FAC)%TEX_SIZE
    lda TunnelAngles+8,y        ; 4
    adc tn_add_rotate           ; 3
    asr #%00011110              ; 2, %TEX_SIZE, /2 for PRECISION_FAC
    sta tn_u                    ; 3
    ; v = ((distances(x+8, y) + add_dist)/PRECISION_FAC)%TEX_SIZE
    lda TunnelDistances+8,y     ; 4, is *8 already
    adc tn_add_distance         ; 3, must be *8 already
    ; /PRECISION_FAC;%TEX_SIZE;*2 can be collapsed into and #%01111000
    and #%11110000              ; 2
    ; get pixel from texture, i.e. a = texture(u, v) = texture(8*v+u)
    ora tn_u                    ; 3
    txs                         ; 2, x=tn_x is needed later again
    tax                         ; 2
    lda TunnelAngles,x          ; 4
    and tn_tex_mask             ; 3

    ; store in top left quadrant
    cmp #1                      ; 2
    ror tn_cur_left_2           ; 5

    ; store rotated in top right quadrant
    tsx                         ; 2, get tn_x as index
    cmp #1                      ; 2
    ror tn_right_2+8+8,x        ; 6, +8 to adjust for loop counter

    ; loop x
    iny                         ; 2, for y*16+x
    inx                         ; 2
    bne .loopX                  ; 2/3
                                ; =?

    ; store constructed left values
    ldx tn_y
    lda tn_cur_left_1
    sta tn_left_1,x
    lda tn_cur_left_2
    sta tn_left_2,x
    ; loop y
    dex
    stx tn_y
    cpx tn_compute_upto
    bne .loopY

    echo "[M]", TunnelComputeTopLoop, "to", *, ": TunnelComputeTopLoop"

    beq RestoreStackAndRts      ; unconditional


; ---------------------------------------------------------------------
; Left bottom, right left, odd frames:
; left_1 bottom -> right_1 top
; left_2 bottom -> right_1 bottom
; ---------------------------------------------------------------------
TunnelComputeBottom SUBROUTINE
TunnelComputeBottomLoop:
.loopY:
    ; calc base index into angles/distances, i.e. y*16 for later use as y*16+x
    ldy TunnelMul16,x
    ; for x = 0 to 7. Use 248 to 255 instead so inx; bne can be used
    ldx #248
.loopX:
    ; ---------- Left_1 ----------
    ; u = ((angles(x, y+8) + add_rotate)/PRECISION_FAC)%TEX_SIZE
    lda TunnelAngles+128,y      ; 4
    adc tn_add_rotate           ; 3
    asr #%00011110              ; 2, %TEX_SIZE, /2 for PRECISION_FAC
    sta tn_u                    ; 3
    ; v = ((distances(x, y+8) + add_dist)/PRECISION_FAC)%TEX_SIZE
    lda TunnelDistances+128,y   ; 4, is *8 already
    adc tn_add_distance         ; 3, must be *8 already
    ; /PRECISION_FAC;%TEX_SIZE;*2 can be collapsed into and #%01111000
    and #%11110000              ; 2
    ; get pixel from texture, i.e. a = texture(u, v) = texture(8*v+u)
    ora tn_u                    ; 3
    txs                         ; 2, x=tn_x is needed later again
    tax                         ; 2
    lda TunnelAngles,x          ; 4
    and tn_tex_mask             ; 3

    ; store in top left quadrant
    cmp #1                      ; 2
    rol tn_cur_left_1           ; 5

    ; store rotated in top right quadrant
    tsx                         ; 2, get tn_x as index
    cmp #1                      ; 2
    rol tn_right_1+8,x          ; 6, +8 to adjust for loop counter

    ; ---------- Left_2 ----------
    ; u = ((angles(x+8, y+8) + add_rotate)/PRECISION_FAC)%TEX_SIZE
    lda TunnelAngles+128+8,y    ; 4
    adc tn_add_rotate           ; 3
    asr #%00011110              ; 2, %TEX_SIZE, /2 for PRECISION_FAC
    sta tn_u                    ; 3
    ; v = ((distances(x+8, y+8) + add_dist)/PRECISION_FAC)%TEX_SIZE
    lda TunnelDistances+128+8,y ; 4, is *8 already
    adc tn_add_distance ; 3, must be *8 already
    ; /PRECISION_FAC;%TEX_SIZE;*2 can be collapsed into and #%01111000
    and #%11110000              ; 2
    ; get pixel from texture, i.e. a = texture(u, v) = texture(8*v+u)
    ora tn_u                    ; 3
    txs                         ; 2, x=tn_x is needed later again
    tax                         ; 2
    lda TunnelAngles,x          ; 4
    and tn_tex_mask             ; 3

    ; store in top left quadrant
    cmp #1                      ; 2
    ror tn_cur_left_2           ; 5

    ; store rotated in top right quadrant
    tsx                         ; 2, get tn_x as index
    cmp #1                      ; 2
    rol tn_right_1+8+8,x        ; 6, +8 to adjust for loop counter

    ; loop x
    iny                         ; 2, for y*16+x
    inx                         ; 2
    bne .loopX                  ; 2/3
                                ; =?

    echo "[M]", TunnelComputeBottomLoop, "to", *, ": TunnelComputeBottomLoop"

    ; store constructed left values
    ldx tn_y
    lda tn_cur_left_1
    sta tn_left_1+8,x
    lda tn_cur_left_2
    sta tn_left_2+8,x
    ; loop y
    dex
    stx tn_y
    cpx tn_compute_upto
    bne .loopY

RestoreStackAndRts:
    ldx tn_stack
    txs
    rts


; =====================================================================
; Kernel
; =====================================================================

; ---------------------------------------------------------------------
; Mask data
; ---------------------------------------------------------------------

; tn_outer_pf_2 is in relief.asm now

PistolSfx:
    dc.b 15, 15, 13, 9, 6, 3, 3
    ; overlap
    ;dc.b 0
PISTOL_LEN = * - PistolSfx + 1
tn_outer_grp:
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %11100000
    dc.b %11111000
    dc.b %11111100
    dc.b %11111110
    dc.b %11111110
    dc.b %01111111
    dc.b %00111111
    dc.b %00011111

    echo "[M]", tn_outer_grp, "to", *, ": tn_outer_grp"

; Accessed starting at address -1, index between 1..4
TriangleGrp:
    dc.b %11000000
    dc.b %11110000
    dc.b %11111100
    ; overlap
;    dc.b %11111111
tn_inner_pf_1:
    dc.b %11111111
    dc.b %11111111
    dc.b %11111111
    dc.b %11111111
    dc.b %11111111
    dc.b %11111110
    dc.b %11111100
    dc.b %11111000
    dc.b %11110000
    dc.b %11110000
    dc.b %11100000
    dc.b %11100000
    dc.b %11000000
    dc.b %11000000
    dc.b %11000000
    dc.b %11000000

    echo "[M]", tn_inner_pf_1, "to", *, ": tn_inner_pf_1"

tn_inner_pf_2:
    dc.b %11111111
    dc.b %11111111
    dc.b %00001111
    ; Overlap
    ;dc.b %00000011
; Instrument Release start indexes into ADSR tables
; Caution: Values are stored with an implicit -1 modifier! To get the
; real index, add 1.
tt_InsReleaseIndexes:
        dc.b $03, $03, $07

        echo "[M]", tn_inner_pf_2, "to", *, ": tn_inner_pf_2"

    ; 0=rl_startPF02, 1=rl_startPF1
ReliefColumnIndexes:
    dc.b 0, 0, 1, 1, 1, 1
    ; overlap
    ; dc.b 0
tn_inner_grp:
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000
    dc.b %00000000

    dc.b %00000000
    dc.b %10000000
    dc.b %11000000
    dc.b %01100000

    echo "[M]", tn_inner_grp, "to", *, ": tn_inner_grp"


; ---------------------------------------------------------------------
; Subroutines for top/bottom inner/outer scanlines
; ---------------------------------------------------------------------

; Enter <@73, returns @65
TunnelTopInner SUBROUTINE
    sta WSYNC                   ; 3
    ; Line 2
    ldx tn_left_1,y             ; 4
    lda tn_inner_pf_1,y         ; 4
    sax PF1                     ; 3
    ldx tn_inner_pf_2,y         ; 4
    lda tn_left_2,y             ; 4
    sax PF2                     ; 3
    ldx tn_inner_grp,y          ; 4
    sax GRP0                    ; 3
    lda tn_right_1,y            ; 4
    sax GRP1                    ; 3
    ldx tn_inner_pf_2,y         ; 4
    SL5
    ; @45
    sax PF2                     ; 3
    ldx tn_right_2,y            ; 4
    lda tn_inner_pf_1,y         ; 4
    ; <@60
    sax PF1                     ; 3
    SL12
    SL9
    ; fall through to TunnelTopOuter

; Enter @4, returns @61
TunnelTopOuter SUBROUTINE
    ; Correct background color
    lda #TN_COL_BACK            ; 2
    sta COLUBK                  ; 3
    ; Line 1
    ldx tn_left_1,y             ; 4
    stx PF1                     ; 3
    ldx tn_outer_pf_2,y         ; 4
    lda tn_left_2,y             ; 4
    sax PF2                     ; 3
    ldx tn_outer_grp,y          ; 4
    sax GRP0                    ; 3
    lda tn_right_1,y            ; 4
    sax GRP1                    ; 3
    ldx tn_outer_pf_2,y         ; 4
    ; @45
    sax PF2                     ; 3
    ldx tn_right_2,y            ; 4
    ; <@60
    stx PF1                     ; 3
    rts                         ; 6


; Enter <@73, returns @65
TunnelBottomInner SUBROUTINE
    sta WSYNC                   ; 3
    ; Line 2
    ldx tn_right_2,y            ; 4
    lda tn_inner_pf_1,y         ; 4
    sax PF1                     ; 3
    ldx tn_inner_pf_2,y         ; 4
    lda tn_right_1,y            ; 4
    sax PF2                     ; 3
    ldx tn_inner_grp,y          ; 4
    sax GRP0                    ; 3
    lda tn_left_2,y             ; 4
    sax GRP1                    ; 3
    ldx tn_inner_pf_2,y         ; 4
    SL5
    ; @45
    sax PF2                     ; 3
    ldx tn_left_1,y             ; 4
    lda tn_inner_pf_1,y         ; 4
    ; <@60
    sax PF1                     ; 3
    ; fall through to TunnelBottomOuter

; Enter <@73, returns @61
TunnelBottomOuter SUBROUTINE
    sta WSYNC
    ldx tn_right_2,y            ; 4
    stx PF1                     ; 3
    ldx tn_outer_pf_2,y         ; 4
    lda tn_right_1,y            ; 4
    sax PF2                     ; 3
    ldx tn_outer_grp,y          ; 4
    sax GRP0                    ; 3
    lda tn_left_2,y             ; 4
    sax GRP1                    ; 3
    ldx tn_outer_pf_2,y         ; 4
    SL9
    ; @45
    sax PF2                     ; 3
    ldx tn_left_1,y             ; 4
    ; <@60
    stx PF1                     ; 3
    rts


; ---------------------------------------------------------------------
; Kernel
; ---------------------------------------------------------------------

TunnelKernel    SUBROUTINE
    ; numFrame is in a already
    jsr DoJitter

    sta WSYNC
    lda #TN_COL_LIGHT           ; 2
    sta COLUBK                  ; 3
    ; TN_COL_LIGHT = 2
    sta COLUPF                  ; 3

    lda #%00001000              ; 2, Mirror player
    sta REFP0                   ; 3
    lda #TN_COL_DARK            ; 2
    sta COLUP0                  ; 3
    sta COLUP1                  ; 3
    lda #%00000111              ; 2, Quad-sized player, missile size 4
    sta NUSIZ0                  ; 3
    sta.w NUSIZ1                ; 4

    ldy #0                      ; 2
    ; @32
    SL12
    SL12
    SL12

    ; enter @68
TunnelTopLoop:
    SL6
    ; @74
    ; ---------- first block ----------
    jsr TunnelTopOuter
    jsr TunnelTopInner      ; also does TunnelTopOuter
    SL6
    SL7

    ; ---------- second block ----------
    jsr TunnelTopOuter
    jsr TunnelTopInner      ; also does TunnelTopOuter

    ; loop
    iny                         ; 2
    cpy #15                     ; 2
    bcc TunnelTopLoop           ; 2/3

    echo "[M]", TunnelTopLoop, "to", *, ": TunnelTopLoop"

    ; Enter <@73
TunnelBottomLoop:
    ; ---------- first block ----------
    jsr TunnelBottomOuter
    jsr TunnelBottomInner   ; also does ...Outer

    ; ---------- second block ----------
    jsr TunnelBottomOuter
    jsr TunnelBottomInner   ; also does ...Outer

    ; loop
    dey                         ; 2
    bpl TunnelBottomLoop

    echo "[M]", TunnelBottomLoop, "to", *, ": TunnelBottomLoop"

    iny                         ; now 0
    sty PF1
    sty PF2
    sty GRP0
    sty GRP1
    ldx #TN_COL_LIGHT
    stx COLUBK

    jsr EnsureNormalMusic

    ; Create first lines of next tunnel
    inc tn_numFrame
    lda tn_numFrame
    ldx aggression
    bne .slow
    lsr
.slow:
    and #31
    sta tn_add_rotate
    lda tn_numFrame
    and #%01111100
    asl
    sta tn_add_distance

    ldx #7
    stx tn_y
    dex                 ; =6
    jmp TunnelCompute

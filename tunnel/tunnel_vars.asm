TN_COL_BACK     = $64
TN_COL_DARK     = $48 ;$66
TN_COL_LIGHT    = $02

    ORG     START_VAR_PARTS
tn_numFrame             ds 1

    echo "Tunnel permanent RAM left: ",($100 - *)

tn_left_1               ds 16   ; framebuffer
tn_left_2               ds 16
tn_right_1              ds 16
tn_right_2              ds 16

tn_tmp                  ds 1
tn_stack                ds 1
tn_tex_mask             ds 1
tn_y                    ds 1
tn_u                    ds 1
tn_cur_left_1           ds 1    ; current left values for ror-constructing
tn_cur_left_2           ds 1
tn_compute_upto         ds 1
tn_add_rotate           ds 1    ; rotation value *8
tn_add_distance         ds 1    ; zoom value

    echo "Tunnel stack RAM left: ",($100 - *)

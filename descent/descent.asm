    echo ""
    echo "------------------------------------------------------------"
    echo "DESCENT:"

; =====================================================================
; VBlank
; =====================================================================

; ---------------------------------------------------------------------
; Overscan main
; ---------------------------------------------------------------------
DescentOverscan    SUBROUTINE
    ; numFrame is in a already

    ; check if we have to init
    ldx de_curEndFrame
    bne .noInit
    stx de_curPart
    ldy #31
    sty de_curEndFrame
    dec aggression          ; cancel last relief inc
.noInit:

    ; check if current part has ended
    lda de_curEndFrame
    cmp #63
    bcc .notTooSmall
    lda #2
.notTooSmall:
    cmp numFrame
    bne .noNextPart
    ; New part!
    lda #0
    sta numFrame
    sta numFrameHi
    ldy de_curPart
    iny
    cpy #3
    bne .noSpeedup
    ; New round!
    tay                     ; = 0
    ldx de_curEndFrame
    ; must not result in 0 (= init!)
    dex
    dex
    cpx #211                ; end of descent?
    bne .noDescentEnd
    inc numPart
    inc curPartIndex
    rts
.noDescentEnd:
    stx de_curEndFrame
.noSpeedup:
    sty de_curPart
.noNextPart:

    ; Check for Tunnel
    lda de_curPart
    bne BackToMain
    jmp TunnelOverscan

DescentVBlank:
    ; Dispatch current part's VBlank
    ldx de_curPart
    ldy VBlankLo+2,x
    lda VBlankHi+2,x
    ; fall through to SetJmpPointer

; ---------------------------------------------------------------------
; Push pointer to stack and rts. Subroutine will return to original jsr.
; IN:
;   a   ptr hi
;   y   ptr lo
; ---------------------------------------------------------------------
SetJmpPointer   SUBROUTINE
    pha
    tya
    pha
    lda numFrame
BackToMain:
    rts


; =====================================================================
; Kernel
; =====================================================================

DescentKernel    SUBROUTINE
    ; Dispatch current part's Kernel
    ldx de_curPart
    ldy KernelLo+2,x
    lda KernelHi+2,x
    bne SetJmpPointer

    echo ""
    echo "------------------------------------------------------------"
    echo "RELIEF:"

; =====================================================================
; Data
; =====================================================================

; bit 0 is bit 3 of script data
ScriptHigherBit:
ReliGfx:
    dc.b %00000000 | (PART_NOTHING >> 3)    ;  0

    dc.b %01110000 | (PART_LIGHTNING >> 3)  ;  1
    dc.b %00100000 | (PART_TUNNEL >> 3)     ;  2
    dc.b %00100000 | (PART_LIGHTNING >> 3)  ;  3
    dc.b %00100000 | (PART_TRIANGLE >> 3)   ;  4
    dc.b %01110000 | (PART_LIGHTNING >> 3)  ;  5

    dc.b %00000000 | (PART_PARALLAX >> 3)   ;  6

    dc.b %11111000 | (PART_LIGHTNING >> 3)  ;  7
    dc.b %10000000 | (PART_GAME >> 3)       ;  8
    dc.b %10000000 | (PART_RELIEF >> 3)     ;  9
    dc.b %10000000 | (PART_LIGHTNING >> 3)  ; 10
    dc.b %10000000 | (PART_TUNNEL >> 3)     ; 11

    dc.b %00000000 | (PART_LIGHTNING >> 3)  ; 12

    dc.b %11111000 | (PART_TRIANGLE >> 3)   ; 13
    dc.b %10000000 | (PART_LIGHTNING >> 3)  ; 14
    dc.b %11110000 | (PART_PARALLAX >> 3)   ; 15
    dc.b %10000000 | (PART_LIGHTNING >> 3)  ; 16
    dc.b %11111000 | (PART_GAME >> 3)       ; 17

    dc.b %00000000 | (PART_RELIEF >> 3)     ; 18

    dc.b %10001000 | (PART_LIGHTNING >> 3)  ; 19
    dc.b %10010000 | (PART_TUNNEL >> 3)     ; 20
    dc.b %11110000 | (PART_LIGHTNING >> 3)  ; 21
    dc.b %10001000 | (PART_TRIANGLE >> 3)   ; 22
    dc.b %11110000 | (PART_LIGHTNING >> 3)  ; 23

VeIfGfx:
    ; "VE"
    dc.b %00000000 | (PART_PARALLAX >> 3)   ; 24

    dc.b %11111000 | (PART_LIGHTNING >> 3)  ; 25
    dc.b %10000000 | (PART_GAME >> 3)       ; 26
    dc.b %11110000 | (PART_RELIEF >> 3)     ; 27
    dc.b %10000000 | (PART_DESCENT >> 3)    ; 28
    dc.b %11111000 | (PART_REALITY >> 3)    ; 29

    dc.b %00000000 | (PART_WHITENOISE >> 3) ; 30

    dc.b %00100000 | (PART_NOTHING >> 3)    ; 31
ScriptLowerBits:
    dc.b %01010000 | (PART_NOTHING & 7)     ;  0
    dc.b %01010000 | (PART_LIGHTNING & 7)   ;  1
    dc.b %10001000 | (PART_TUNNEL & 7)      ;  2
    dc.b %10001000 | (PART_LIGHTNING & 7)   ;  3

    ; "IF"
    dc.b %00000000 | (PART_TRIANGLE & 7)    ;  4

    dc.b %10000000 | (PART_LIGHTNING & 7)   ;  5
    dc.b %10000000 | (PART_PARALLAX & 7)    ;  6
    dc.b %11110000 | (PART_LIGHTNING & 7)   ;  7
    dc.b %10000000 | (PART_GAME & 7)        ;  8
    dc.b %11111000 | (PART_RELIEF & 7)      ;  9

    dc.b %00000000 | (PART_LIGHTNING & 7)   ; 10

    dc.b %11111000 | (PART_TUNNEL & 7)      ; 11
    dc.b %10000000 | (PART_LIGHTNING & 7)   ; 12
    dc.b %11110000 | (PART_TRIANGLE & 7)    ; 13
    dc.b %10000000 | (PART_LIGHTNING & 7)   ; 14
    dc.b %11111000 | (PART_PARALLAX & 7)    ; 15

LightningGlyphs:
    ; A
    dc.b %01100000 | (PART_LIGHTNING & 7)   ; 16
    dc.b %10010000 | (PART_GAME & 7)        ; 17
    dc.b %11110000 | (PART_RELIEF & 7)      ; 18
    dc.b %10010000 | (PART_LIGHTNING & 7)   ; 19
    dc.b %10010000 | (PART_TUNNEL & 7)      ; 20

    ; C
    dc.b %01110000 | (PART_LIGHTNING & 7)   ; 21
    dc.b %10000000 | (PART_TRIANGLE & 7)    ; 22
    dc.b %10000000 | (PART_LIGHTNING & 7)   ; 23
    dc.b %10000000 | (PART_PARALLAX & 7)    ; 24
    dc.b %01110000 | (PART_LIGHTNING & 7)   ; 25

    ; E
    dc.b %11110000 | (PART_GAME & 7)        ; 26
    dc.b %10000000 | (PART_RELIEF & 7)      ; 27
    dc.b %11100000 | (PART_DESCENT & 7)     ; 28
    dc.b %10000000 | (PART_REALITY & 7)     ; 29
    dc.b %11110000 | (PART_WHITENOISE & 7)  ; 30

    ; F
    dc.b %11110000 | (PART_NOTHING & 7)     ; 31
    dc.b %10000000
    dc.b %11100000
    dc.b %10000000
    dc.b %10000000

    ; G
    dc.b %01110000
    dc.b %10000000
    dc.b %10110000
    dc.b %10010000
    dc.b %01100000

    ; H
    dc.b %10010000
PartDurationLo:
    dc.b %10010000 | ((DUR_PART00>>3) & 7)
    dc.b %11110000 | ((DUR_PART01>>3) & 7)
    dc.b %10010000 | ((DUR_PART02>>3) & 7)
    dc.b %10010000 | ((DUR_PART03>>3) & 7)

    ; I
    dc.b %10000000 | ((DUR_PART04>>3) & 7)
    dc.b %10000000 | ((DUR_PART05>>3) & 7)
    dc.b %10000000 | ((DUR_PART06>>3) & 7)
    dc.b %10000000 | ((DUR_PART07>>3) & 7)
    dc.b %10000000 | ((DUR_PART08>>3) & 7)

    ; L
    dc.b %10000000 | ((DUR_PART09>>3) & 7)
    dc.b %10000000 | ((DUR_PART10>>3) & 7)
    dc.b %10000000 | ((DUR_PART11>>3) & 7)
    dc.b %10000000 | ((DUR_PART12>>3) & 7)
    dc.b %11110000 | ((DUR_PART13>>3) & 7)

    ; M
    dc.b %10001000 | ((DUR_PART14>>3) & 7)
    dc.b %11011000 | ((DUR_PART15>>3) & 7)
    dc.b %10101000 | ((DUR_PART16>>3) & 7)
    dc.b %10001000 | ((DUR_PART17>>3) & 7)
    dc.b %10001000 | ((DUR_PART18>>3) & 7)

    ; N
    dc.b %10010000 | ((DUR_PART19>>3) & 7)
    dc.b %11010000 | ((DUR_PART20>>3) & 7)
    dc.b %10110000 | ((DUR_PART21>>3) & 7)
    dc.b %10010000 | ((DUR_PART22>>3) & 7)
    dc.b %10010000 | ((DUR_PART23>>3) & 7)

    ; O
    dc.b %01100000 | ((DUR_PART24>>3) & 7)
    dc.b %10010000 | ((DUR_PART25>>3) & 7)
    dc.b %10010000 | ((DUR_PART26>>3) & 7)
    dc.b %10010000 | ((DUR_PART27>>3) & 7)
    dc.b %01100000 | ((DUR_PART28>>3) & 7)

    ; P
    dc.b %11100000 | ((DUR_PART29>>3) & 7)
    dc.b %10010000 | ((DUR_PART30>>3) & 7)
    dc.b %11100000 | ((DUR_PART31>>3) & 7)
PartDurationHi:
    dc.b %10000000 | ((DUR_PART00>>6) & 7)
    dc.b %10000000 | ((DUR_PART01>>6) & 7)

    ; R
    dc.b %11100000 | ((DUR_PART02>>6) & 7)
    dc.b %10010000 | ((DUR_PART03>>6) & 7)
    dc.b %11100000 | ((DUR_PART04>>6) & 7)
    dc.b %10100000 | ((DUR_PART05>>6) & 7)
    dc.b %10010000 | ((DUR_PART06>>6) & 7)

    ; S
    dc.b %01110000 | ((DUR_PART07>>6) & 7)
    dc.b %10000000 | ((DUR_PART08>>6) & 7)
    dc.b %01100000 | ((DUR_PART09>>6) & 7)
    dc.b %00010000 | ((DUR_PART10>>6) & 7)
    dc.b %11100000 | ((DUR_PART11>>6) & 7)

    ; T
    dc.b %11111000 | ((DUR_PART12>>6) & 7)
    dc.b %00100000 | ((DUR_PART13>>6) & 7)
    dc.b %00100000 | ((DUR_PART14>>6) & 7)
    dc.b %00100000 | ((DUR_PART15>>6) & 7)
    dc.b %00100000 | ((DUR_PART16>>6) & 7)

    ; U
    dc.b %10010000 | ((DUR_PART17>>6) & 7)
    dc.b %10010000 | ((DUR_PART18>>6) & 7)
    dc.b %10010000 | ((DUR_PART19>>6) & 7)
    dc.b %10010000 | ((DUR_PART20>>6) & 7)
    dc.b %01100000 | ((DUR_PART21>>6) & 7)

    ; W
    dc.b %10001000 | ((DUR_PART22>>6) & 7)
    dc.b %10001000 | ((DUR_PART23>>6) & 7)
    dc.b %10101000 | ((DUR_PART24>>6) & 7)
    dc.b %11011000 | ((DUR_PART25>>6) & 7)
    dc.b %10001000 | ((DUR_PART26>>6) & 7)

    ; Y
    dc.b %10001000 | ((DUR_PART27>>6) & 7)
    dc.b %01010000 | ((DUR_PART28>>6) & 7)
    dc.b %00100000 | ((DUR_PART29>>6) & 7)
    dc.b %00100000 | ((DUR_PART30>>6) & 7)
    dc.b %00100000 | ((DUR_PART31>>6) & 7)

    ; Init values for RAM vars, copied during first frame
ReliefStartParameters:
    ; rl_mode
    dc.b RL_MODE_RELIEF
    ; rl_startPF02 and rl_startPF1
    dc.b %11110000          ; Column 1, 0, -, 6
    dc.b %11111111          ; Column 2, 3, 4, 5
    ; rl_startCounts
    dc.b 4, 7, 10, 13, 16, 19, 1
    ; rl_startCountsLo
    ; 7th 255 not needed, as only 0 gets added as lo byte anyway
    ; also: overlapped start of tn_outer_pf_2
tn_outer_pf_2:
    dc.b 255, 255, 255, 255, 255, 255

    ;dc.b %11111111
    ;dc.b %11111111
    ;dc.b %11111111
    ;dc.b %11111111
    ;dc.b %11111111
    ;dc.b %11111111
    dc.b %11111111
    dc.b %11111111
    dc.b %11111111
    dc.b %11111111
    dc.b %11111111
    dc.b %11111111
    dc.b %11111111
    dc.b %01111111
    dc.b %00111111
    dc.b %00011111

    echo "[M]", tn_outer_pf_2, "to", *, ": tn_outer_pf_2"

    ; Adds for column counts and start values in case of wrap
ReliefCountAddLo:
    dc.b 146, 122, 98, 73, 49, 24
    ; overlap
    ; dc.b 0
CrosshairGfx:
    dc.b 0
    dc.b %00000001
    dc.b %00000111
    dc.b %00001001
    dc.b %00010001
    dc.b %00010000
    dc.b %00111100
    dc.b %00010000
    dc.b %00010001
    dc.b %00001001
    dc.b %00000111
    dc.b %00000001


    ; ReliefColumnIndexes is now in tunnel.asm

    ; Column EOR masks for PFx
ReliefColumnMasks:
    dc.b %00110000
    dc.b %11000000
    dc.b %11000000
    dc.b %00110000
    dc.b %00001100
    dc.b %00000011
    dc.b %00000011
    ; RlModeChangeTimestamps now in triangle.asm

; =====================================================================
; VBlank
; =====================================================================

ReliefVBlank    SUBROUTINE
    ; numFrame is in a already
    bne .noFirstInit
    ldx #15
.initParamsLoop:
    ldy ReliefStartParameters,x
    sty rl_mode,x
    dex
    bpl .initParamsLoop
    ; Increase aggression for the next round of kernels
    inc aggression
.noFirstInit:

    ; Gfx init
    ; Already fine from Game
    ;lda #%00000001          ; mirror playfield
    ;sta CTRLPF

    lda #RL_COL_BACK
    and rl_mode
    sta COLUBK
    lda #RL_COL_RAYS
    and rl_mode
    sta COLUPF
    lda #RL_COL_RELIEF
    ldy #3*RL_VE_IF_HEIGHT-1
    bit rl_mode                 ; v flag needed later as well
    bvs .reliefMode
    lda #RL_COL_RELIVE
    ldy #2*RL_VE_IF_HEIGHT-1
.reliefMode:
    sta COLUP0
    sta COLUP1

    ; Copy word. y is index into correct last two letters for mode
    ldx #2*RL_VE_IF_HEIGHT
.copyLoop:
    ; copy "RELI"
    lda ReliGfx-1,x
    ; mask out script data
    and #%11111000
    sta rl_gfxData+2*RL_VE_IF_HEIGHT-1,x
    ; copy "EF/VE"
    lda VeIfGfx-RL_VE_IF_HEIGHT,y
    and #%11111000
    sta rl_gfxData-1,x
    ; copy start parameters
    lda rl_startPF02-1,x
    sta rl_curPF02-1,x
    dey
    dex
    bne .copyLoop

    lda #%00000101
    sta NUSIZ0
    lda #74                     ; xpos
    ldy #87                     ; ypos
    bvs .reliefMode3            ; v flag still valid from above
    lda rl_counts+4
    and #1
    adc #86
    tay
    lda rl_counts+3
    and #1
    adc #73
.reliefMode3:
    sty rl_gfxPos
    ; x is still 0 from loop above
    FINEPOS
    sta WSYNC
    sta HMOVE

    ; Check for relief/relive change
    ldy rl_nextModeIndex
    lda RlModeChangeTimestamps,y
    bne .noEndPart
    iny
.noEndPart:
    cmp numFrame
    bne .noModeChange
    iny
    lda rl_mode
    eor #RL_MODE_EOR
    sta rl_mode
.noModeChange:
    sty rl_nextModeIndex

    ; Parallax init, ctd.
    lda rl_curPF02
    sta PF0
    and #%00001111
    sta PF2
    lda rl_curPF1
    sta PF1

    jsr tt_Player

    jsr ReliefAdvanceColumns
    lda rl_mode
    bmi ReliefOverscan
    ; fall through to ReliefAdvanceColumns

; ---------------------------------------------------------------------
; Advance parallax columns
; Here so that the rts at the end is reachable with the bmi above.
; ---------------------------------------------------------------------
ReliefAdvanceColumns    SUBROUTINE
    ldx #6
.moveColumnsLoop:
    lda rl_startCountsLo,x
    sec
    sbc ReliefCountAddLo,x
    sta rl_startCountsLo,x
    lda rl_startCounts,x
    sbc #1
    sta rl_startCounts,x
    bpl .noChange
    ; Restart count and change column bit
    ; StartCount is RL_START_0-2*x (assumes RL_START_x decreases by 2 each step)
    txa
    asl
    eor #$ff
    adc #RL_START_0+1           ; carry is clear from underflow above
    sta rl_startCounts,x
    ldy ReliefColumnIndexes,x
    lda rl_startPF02,y
    eor ReliefColumnMasks,x
    sta rl_startPF02,y
    lda #0
    sta rl_startCountsLo,x
.noChange:
    dex
    bpl .moveColumnsLoop

ReliefOverscan:
    rts


; =====================================================================
; Kernel
; =====================================================================

ReliefKernel    SUBROUTINE
    ldy #107
ReliefKernelLoop:
    sta WSYNC                   ; 3

    ; @0
    lda #2*RL_SPRITE_HEIGHT-1   ; 2
    dcp rl_gfxPos               ; 5
    bcc .skipDraw               ; 2/3
    lda rl_gfxPos               ; 3
    lsr                         ; 2
    tax                         ; 2
    lda rl_gfxData+RL_VE_IF_HEIGHT,x            ; 4
    sta GRP0                    ; 3
.skipDraw:

    ; @13
    lda rl_curPF02              ; 3

    ; @26, column 0
    ldx rl_counts+0             ; 3
    dex                         ; 2
    bpl .noChange0              ; 2/3
    eor #%00110000              ; 2
    ldx #RL_START_0             ; 2
.noChange0:
    stx rl_counts+0             ; 3

    ; @40, column 1
    ldx rl_counts+1             ; 3
    dex                         ; 2
    bpl .noChange1              ; 2/3
    eor #%11000000              ; 2
    ldx #RL_START_1             ; 2
.noChange1:
    stx rl_counts+1             ; 3

    ; @54, column 6
    ldx rl_counts+6             ; 3
    dex                         ; 2
    bpl .noChange6              ; 2/3
    eor #%00000011              ; 2
    ldx #RL_START_6             ; 2
.noChange6:
    stx rl_counts+6             ; 3

    ; @68, store PF0/2 and retrieve PF1
    sta rl_curPF02              ; 3
    sta PF0                     ; 3
    and #%00001111              ; 2
    sta PF2                     ; 3
    lda rl_curPF1               ; 3

    ; @82, column 2
    ldx rl_counts+2             ; 3
    dex                         ; 2
    bpl .noChange2              ; 2/3
    eor #%11000000              ; 2
    ldx #RL_START_2             ; 2
.noChange2:
    stx rl_counts+2             ; 3

    ; @96, column 3
    ldx rl_counts+3             ; 3
    dex                         ; 2
    bpl .noChange3              ; 2/3
    eor #%00110000              ; 2
    ldx #RL_START_3             ; 2
.noChange3:
    stx rl_counts+3             ; 3

    ; @110, column 4
    ldx rl_counts+4             ; 3
    dex                         ; 2
    bpl .noChange4              ; 2/3
    eor #%00001100              ; 2
    ldx #RL_START_4             ; 2
.noChange4:
    stx rl_counts+4             ; 3

    ; @124, column 5
    ldx rl_counts+5             ; 3
    dex                         ; 2
    bpl .noChange5              ; 2/3
    eor #%00000011              ; 2
    ldx #RL_START_5             ; 2
.noChange5:
    stx rl_counts+5             ; 3

    ; @138, store PF1
    sta rl_curPF1               ; 3
    sta PF1                     ; 3

    ; @144, loop
    dey                         ; 2
    bne ReliefKernelLoop        ; 2/3
    ; Sum; 152 cycles (incl. wsync), 0 free

    echo "[M]", ReliefKernelLoop, "to", *, ": ReliefKernelLoop"

    rts

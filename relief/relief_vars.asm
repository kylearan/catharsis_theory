RL_SPRITE_HEIGHT    = 36    ; Height of "RELIEF"/"RELIVE" GFX
RL_RELI_HEIGHT      = 24    ; "RELI" height
RL_VE_IF_HEIGHT     = 12

RL_START_0          = 32    ; Height of rays per column
RL_START_1          = 30
RL_START_2          = 28
RL_START_3          = 26
RL_START_4          = 24
RL_START_5          = 22
RL_START_6          = 20

RL_MODE_RELIEF  = %11111111
RL_MODE_RELIVE  = %00001111
RL_MODE_EOR     = %11110000

RL_COL_BACK     = $b6
RL_COL_RAYS     = $ba
RL_COL_RELIEF   = $0c
RL_COL_RELIVE   = $64

    ORG     START_VAR_PARTS
rl_mode                 ds 1    ; RL_MODE_RELIEF or RL_MODE_RELIVE. Must be before rl_startPF02!
rl_startPF02            ds 1    ; Combines columns 0, 1, 6 (PF0 bits 4..7 and PF2 bits 0..1)
rl_startPF1             ds 1
rl_startCounts          ds 7
rl_startCountsLo        ds 7

    echo "Relief permanent RAM left: ",($100 - *)

rl_tmp                  ds 1
rl_ptr                  ds 2
rl_gfxPos               ds 1    ; SkipDraw pos of text
; --> The following must be in the same order
rl_curPF02              ds 1    ; Combines columns 0, 1, 6 (PF0 bits 4..7 and PF2 bits 0..1)
rl_curPF1               ds 1
rl_counts               ds 7    ; counts for parallax columns
rl_gfxData              ds RL_SPRITE_HEIGHT+RL_VE_IF_HEIGHT
; <---

    echo "Relief stack RAM left: ",($100 - *)

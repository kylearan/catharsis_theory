    echo ""
    echo "------------------------------------------------------------"
    echo "REALITY:"

; =====================================================================
; Kernel
; =====================================================================

RealityKernel    SUBROUTINE
    ; Re-use potential init from game, also does pistol
    jsr GameDoGuns

    ; Do pistol
    lda numFrame
    ldy ga_curPistolFrame
    ldx ga_nextPistolShot
    cmp PistolTimes,x
    bne .noNewShot
    inc ga_nextPistolShot
    ldy #0
.noNewShot:
    ldx PistolSfx,y
    stx AUDV1
    beq .shotEnd
    iny
.shotEnd:
    sty ga_curPistolFrame

    rts

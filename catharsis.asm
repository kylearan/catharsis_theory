    processor 6502

    include vcs.h
    include macros.h
    include common.h

MUSIC                   = 2
START_PART              = 0
START_AGGRO             = 0


; TODO:
; - !!!Check scanline count everywhere!!!
; - Investigate wobble in Parallax: Probably jitter color switch
; - Investigate tunnel "line" artifacts

; Optimization:
; - Size: Can tables be interleaved with code, if data=code at start or end?
; - Size: Cut out words before Game
; - Size: Remove jitter
; - Size: Re-order parts so that Tunnel is 0, and remove CMP #TUNNEL
; - Size: Re-order parts so that in ptr table, return pointers are at start
;   and end so they can overlap
; - Size: Count down in Triangle kernel to save the cpx instruction(s)


; =====================================================================
; Constants
; =====================================================================

TIM_OVERSCAN    = 50    ; TIM64T,  3200 cycles = ~ 42 scanlines
TIM_VBLANK      = 61    ; TIM64T,  3904 cycles = ~ 51 scanlines
TIM_KERNEL      = 17    ; T1024T, 17408 cycles = ~229 scanlines


; =====================================================================
; Variables
; =====================================================================

    echo ""
    echo "------------------------------------------------------------"
    echo "VARIABLES:"

    SEG.U   variables
    ORG $80

; ---------------------------------------------------------------------
; Global permanents
; ---------------------------------------------------------------------

    ; ---------- Permanent vars ----------
numPart                 ds 1
numFrame                ds 1
numFrameHi              ds 1
curPartIndex            ds 1
aggression              ds 1    ; Gets increased in Relief init
ln_currentGlyph         ds 1    ; index into first glyph of current word
ln_lightningType        ds 1    ; index into LightningSeeds, also numFrameHi in high bits
rl_nextModeIndex        ds 1
de_curPart              ds 1    ; index of current descent part
de_curEndFrame          ds 1    ; at which numFrame the current part ends

    ; ---------- Music player ----------

    include "music/music_vars_permanent.asm"

    echo "Global permanent RAM left: ",($100 - *)
START_VAR_PARTS     = *


; ---------------------------------------------------------------------
; Part permanents
; ---------------------------------------------------------------------

    include "tunnel/tunnel_vars.asm"
    include "lightning/lightning_vars.asm"
    include "triangle/triangle_vars.asm"
    include "parallax/parallax_vars.asm"
    include "game/game_vars.asm"
    include "reality/reality_vars.asm"
    include "relief/relief_vars.asm"
    include "descent/descent_vars.asm"
    include "whitenoise/whitenoise_vars.asm"

    SEG Bank0
    ORG $f000


; =====================================================================
; SUBROUTINES
; =====================================================================

    include "tunnel/tunnel.asm"
    include "lightning/lightning.asm"
    include "triangle/triangle.asm"
    include "parallax/parallax.asm"
    include "game/game.asm"
    include "reality/reality.asm"
    include "relief/relief.asm"
    include "descent/descent.asm"
    include "whitenoise/whitenoise.asm"

    include "shared/utility.asm"
    include "shared/script.asm"


; =====================================================================
; Start of code
; =====================================================================

Start   SUBROUTINE
    ; Clear zeropage
    cld
    ldx #0
    txa
.clearLoop:
    dex
    tsx
    pha
    bne .clearLoop

    IF START_PART != 0
        lda #START_PART
        sta numPart
        lda #START_AGGRO
        sta aggression
    ENDIF


; =====================================================================
; MAIN LOOP
; =====================================================================

MainLoop:

; ---------------------------------------------------------------------
; Overscan
; ---------------------------------------------------------------------

Overscan    SUBROUTINE
    sta WSYNC
    lda #2
    sta VBLANK
    lda #TIM_OVERSCAN
    sta TIM64T

    ; Construct index of current part
    ldx numPart
    lda ScriptHigherBit,x
    lsr
    lda ScriptLowerBits,x
    and #7
    bcc .noBit3
    ora #%00001000
.noBit3:
    sta curPartIndex

    cmp #PART_TUNNEL
    bne .noTunnel
    jsr TunnelOverscan
    bne .waitForIntim               ; unconditional
.noTunnel:
    cmp #PART_DESCENT
    bne .waitForIntim
    jsr DescentOverscan

.waitForIntim
    lda INTIM
    bne .waitForIntim


; ---------------------------------------------------------------------
; VBlank
; ---------------------------------------------------------------------

VBlank  SUBROUTINE

    lda #%1110
.vsyncLoop:
    sta WSYNC
    sta VSYNC
    lsr
    bne .vsyncLoop
    lda #2
    sta VBLANK
    lda #TIM_VBLANK
    sta TIM64T

    ; construct and call subroutine
    ldx curPartIndex
    ldy VBlankLo,x
    lda VBlankHi,x
    jsr SetJmpPointer


.waitForVBlank:
    lda INTIM
    bne .waitForVBlank
    sta WSYNC
    sta VBLANK


; ---------------------------------------------------------------------
; Kernel
; ---------------------------------------------------------------------

Kernel  SUBROUTINE
    lda #TIM_KERNEL
    sta T1024T

    ; construct and call subroutine
    ldx curPartIndex
    ldy KernelLo,x
    lda KernelHi,x
    jsr SetJmpPointer

    ; Scripting
    inc numFrame
    bne .notZero
    inc numFrameHi
    inc numFrame                ; must not be 0 except for init
.notZero:
    ; Check for end of part
    ; Construct 6-bit value of target timestamp
    ldx numPart
    lda PartDurationHi,x
    and #%00000111
    asl
    asl
    asl
    sta curPartIndex            ; tmp
    lda PartDurationLo,x
    and #%00000111
    ora curPartIndex
    sta curPartIndex
    ; Construct 6-bit value of current 9-bit frame counter
    lda numFrameHi
    ror
    lda numFrame
    ror
    lsr
    lsr
    cmp curPartIndex
    bne .noNextPart
    ; Next part!
    inc numPart
    lda #0
    sta numFrame
    sta numFrameHi
.noNextPart:

.waitForIntim:
    lda INTIM
    bne .waitForIntim

    jmp MainLoop


; =====================================================================
; Music
; =====================================================================

    echo ""
    echo "------------------------------------------------------------"
    echo "MUSIC:"

    IF MUSIC = 1
        include "music/music_trackdata_1.asm"
    ENDIF
    IF MUSIC = 2
        include "music/music_trackdata_2.asm"
    ENDIF
    IF MUSIC = 3
        include "music/music_trackdata_3.asm"
    ENDIF


; =====================================================================
; Vectors
; =====================================================================

    echo ""
    echo "------------------------------------------------------------"
    echo "Rom size:", (* - $f000 + 4), "bytes (", ($fffc - *), "bytes left )"

    ORG     $fffc
    .word   Start
    .word   FinePosObject

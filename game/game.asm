    echo ""
    echo "------------------------------------------------------------"
    echo "GAME:"

; =====================================================================
; Data
; =====================================================================

GAME_COL_HUD        = $90
GAME_COL_HUD_BG     = $92
GAME_COL_SKY        = $de
GAME_GROUND_LINE    = 24
GAME_COL_GROUND     = $20
GAME_COL_CROSSHAIR  = $04


CROSSHAIR_HEIGHT = 12
    ; CrosshairGfx is now in relief.asm

ShotgunTimes:
    dc.b 60, 75, 110, 125, 140, 195, 210

; PistolTimes is now in lightning.asm

; PistolSfx is now in tunnel.asm
PISTOL_FREQ = 13

SHOTGUN_FREQ = 16
; ShotgunSfx is in tunnel.asm now


; =====================================================================
; Subroutines
; =====================================================================

GameWasteThenBar    SUBROUTINE
    jsr WasteScanlines

GameDisplayBar  SUBROUTINE
    sta WSYNC
    lda #$ff
    sta PF0
    sta PF1
    sta PF2
    ldx #6
    jsr WasteScanlines
    ; x is 0
    lda #%00010000
    sta WSYNC
    sta PF0
    stx PF1
    stx PF2
    rts


; Also used by Reality
GameDoGuns SUBROUTINE
    ; numFrame is in a
    bne .noInit
    ; Sound
    sta AUDV0
    sta AUDV1
    sta ga_nextShotgunShot
    sta ga_nextPistolShot
    ; Start relief music
    sta tt_cur_note_index_c0
    sta tt_cur_note_index_c1
    sta tt_cur_pat_index_c0
    sta tt_timer
    ldx #MUSIC_RELIEF_1
    stx tt_cur_pat_index_c1
    ldx #(SHOTGUN_LEN - 1)
    stx ga_curShotgunFrame
    ; =8
    stx AUDC0
    stx AUDC1
    ldx #(PISTOL_LEN - 1)
    stx ga_curPistolFrame
.noInit:

    ; numFrame is in a
    ; Handle shotgun sfx
    ldy ga_curShotgunFrame
    ldx ga_nextShotgunShot
    cmp ShotgunTimes,x
    bne .noNewShot
    ; New shot
    inc ga_nextShotgunShot
    ldy #0
.noNewShot:
    ldx ShotgunSfx,y
    stx AUDV0
    beq .shotEnd
    iny
.shotEnd:
    sty ga_curShotgunFrame

    lda #SHOTGUN_FREQ
    sta AUDF0
    lda #PISTOL_FREQ
    sta AUDF1

    rts


; =====================================================================
; VBlank
; =====================================================================

; ---------------------------------------------------------------------
; VBlank main
; ---------------------------------------------------------------------
GameVBlank    SUBROUTINE
    jsr GameDoGuns

    ; Init Gfx
    lda #%00000001                  ; mirrored
    sta CTRLPF
    lsr                             ; =0
    sta NUSIZ0
    sta NUSIZ1
    tax                             ; =0
    lda #72
    FINEPOS
    lda #79
    FINEPOS
    sta WSYNC
    sta HMOVE
    lda #255
    sta REFP1
    lda #GAME_COL_CROSSHAIR
    sta COLUP0
    sta COLUP1
    lda #GAME_COL_HUD
    sta COLUPF

GameOverscan:
    rts



; =====================================================================
; Kernel
; =====================================================================

GameKernel    SUBROUTINE

    jsr GameDisplayBar
    lda #GAME_COL_SKY
    sta COLUBK
    ldx #75
    jsr WasteScanlines

    ldx #(CROSSHAIR_HEIGHT - 1)
.crosshairLoop:
    sta WSYNC
    sta WSYNC
    lda CrosshairGfx,x
    sta GRP0
    sta GRP1
    dex
    bpl .crosshairLoop

    ldx #18
    jsr WasteScanlines

    ldx #GAME_COL_GROUND+1      ; =33
    stx COLUBK
    jsr GameWasteThenBar

    lda #GAME_COL_HUD_BG
    sta COLUBK
    ldx #36
    jsr GameWasteThenBar

    stx PF0
    stx COLUBK

    stx REFP1
    rts

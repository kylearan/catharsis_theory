PA_NUM_BARS     = 15

    ORG     START_VAR_PARTS
pa_posHi                ds PA_NUM_BARS
pa_posLo                ds PA_NUM_BARS
pa_posAddHi             ds PA_NUM_BARS
pa_posAddLo             ds PA_NUM_BARS

    echo "Parallax permanent RAM left: ",($100 - *)

pa_hmValues             ds PA_NUM_BARS+1
pa_switchColFlags       ds PA_NUM_BARS+1
pa_curCol1              ds 1
pa_curCol2              ds 1
pa_addHi                ds 1
pa_addLo                ds 1
pa_increase             ds 1

    echo "Parallax stack RAM left: ",($100 - *)

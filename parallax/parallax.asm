    echo ""
    echo "------------------------------------------------------------"
    echo "PARALLAX:"

; =====================================================================
; Data
; =====================================================================

; AUDVx and AUDFx ADSR envelope values.
; Each byte encodes the frequency and volume:
; - Bits 7..4: Freqency modifier for the current note ([-8..7]),
;       8 means no change. Bit 7 is the sign bit.
; - Bits 3..0: Volume
; Between sustain and release is one byte that is not used and
; can be any value.
; The end of the release phase is encoded by a 0.
tt_InsFreqVolTable:
; 0+1: Chords
        dc.b $88, $88, $87, $00, $80, $00
; 2: Bass
        dc.b $8f, $00, $8f
        ; Overlap
        ;dc.b $00
; Instrument Attack/Decay start indexes into ADSR tables.
tt_InsADIndexes:
        dc.b $00, $00
        ; Overlap
        ;dc.b $06
BarHeights:
    dc.b 6, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 1, 1, 0
    ; last zero can overlap with following table
BarGradient:
    dc.b 0, 2, 4, 6, 8, 6, 2
    ; Last 0 for symbol flash overlaps with next table

    echo "[M]", BarHeights, "to", *, ": BarHeights"

; Symbol
PA_SYMBOL_HEIGHT = 14
BamGfx_1:
    dc.b %00000000, %00000000, %00000001, %00000001, %01100001, %00111111, %00001000, %00000110
    dc.b %00000010, %00000101, %00001111, %00001100, %00010000, %00100000
BamGfx_2:
    dc.b %10000000, %10000000, %10000010, %11001100, %01011000, %01111000, %00110000, %00010000
    dc.b %00001100, %10111111, %10100000, %01100000, %01100000, %00100000


; =====================================================================
; Subroutines
; =====================================================================

PaPosObjects:   SUBROUTINE
    eor #31
    and #31
    tay                             ; needed for other pos calcs
    ldx #0
    FINEPOS
    tya
    clc
    adc #96
    FINEPOS
    tya
    clc
    adc #8
    FINEPOS
    tya
    clc
    adc #104
    FINEPOS
    ; Delay before HNOVE to avoid jitter
    lda #$ff
    sta ENAM0
    sta ENAM1
    sta GRP0
    sta GRP1

    lda #$60
    ldx #$30
    sta WSYNC
    sta HMOVE
    sta WSYNC
    sta HMCLR
    rts


PaHmCheckColSwitch  SUBROUTINE
    sta HMP0
    sta HMP1
    sta HMM0
    sta HMM1
    ; Check for color switch
    beq .noColorSwitch
    ldy pa_curCol1
    lda pa_curCol2
    sta pa_curCol1
    sty pa_curCol2
.noColorSwitch:
ParallaxOverscan:
    rts


; =====================================================================
; VBlank
; =====================================================================

; ---------------------------------------------------------------------
; VBlank main
; ---------------------------------------------------------------------
ParallaxVBlank    SUBROUTINE
    ; numFrame is in a already
    bne .noInit
    lda #24
    ldx aggression
    beq .noAggro
    asl
.noAggro:
    sta pa_increase

    ldx #(PA_NUM_BARS - 1)
    ;lda #128
    ;ldy #0
.initLoop:
    sta pa_posHi,x
    ;sec
    ;sbc #8
    sta pa_posLo,x
    dex
    bpl .initLoop
.noInit:

    ; Settings
    lda #%00110110          ; 3 copies medium, missile size 8
    sta NUSIZ0
    sta NUSIZ1
    sta PF0
    lda #%00000101          ; Priority, mirrored
    sta CTRLPF

    ; Advance parallax positions and check for reset while doing so
    lda #128
    sta pa_addLo
    asl                     ; =0
    sta pa_addHi

    ldx #(PA_NUM_BARS - 1)
.advancePosLoop:
    ; Move position
    lda pa_posLo,x
    clc
    adc pa_addLo
    sta pa_posLo,x
    lda pa_posHi,x
    adc pa_addHi
    sta pa_posHi,x

    ; Calc hmove value
    sec
    sbc pa_posHi+1,x
    ; Check for color switch
    pha
    clc
    adc #8
    ldy #0
    and #31
    cmp #16
    bcc .noColorSwitch
    iny
.noColorSwitch:
    sty pa_switchColFlags+1,x
    pla
    asl
    asl
    asl
    asl
    sta pa_hmValues+1,x
    ; Increase speed
    lda pa_addLo
    adc pa_increase
    sta pa_addLo
    bcc .noOverflow
    inc pa_addHi
.noOverflow:
    ; loop
    dex
    bpl .advancePosLoop

    jmp tt_Player



; =====================================================================
; Kernel
; =====================================================================

ParallaxKernel    SUBROUTINE

    ; ------------------------------------------------------------
    ; Upper bars
    ; ------------------------------------------------------------

PaUpperBars SUBROUTINE
    ; Position objects and init colors
    lda pa_posHi
    jsr PaPosObjects
    sta pa_curCol2
    stx pa_curCol1

    ldx #0
.loop:
    ldy BarHeights,x
.loop2:
    lda pa_curCol1
    adc BarGradient,y
    adc BarHeights,x
    pha                             ; needed later
    lda pa_curCol2
    sta WSYNC
    sta HMOVE
    adc BarGradient,y
    adc BarHeights,x
    sta COLUBK
    pla
    sta COLUP0
    sta COLUP1
    sta HMCLR
    dey
    bpl .loop2
    lda pa_hmValues+1,x
    eor #$ff
    ldy pa_switchColFlags+1,x
    jsr PaHmCheckColSwitch
    inx
    cpx #PA_NUM_BARS
    bne .loop
    ;sta WSYNC                   ; Uncomment to remove tiny GFX artifact
    jsr CleanUpGfxWithBg

    ; ------------------------------------------------------------
    ; Symbols
    ; ------------------------------------------------------------

PaSymbols   SUBROUTINE

    lda numFrame
    jsr DoJitter
    sta pa_addLo                ; =tmp
    ldx #5
    jsr WasteScanlines

    ; Init symbol objects
    lda #40
    ; x is already 0
    FINEPOS
    lda #48
    FINEPOS
    sta WSYNC
    sta HMOVE

    ; Symbol color
    jsr GetFlashCol
    sta COLUP0
    sta COLUP1

    ldx #(PA_SYMBOL_HEIGHT - 1)
.symbolLoop:
    sta WSYNC
    sta WSYNC
    lda BamGfx_1,x
    sta GRP0
    lda BamGfx_2,x
    sta GRP1
    dex
    bpl .symbolLoop
    jsr CleanUpGfx

    lda #4
    ; carry is clear
    sbc pa_addLo
    tax
    jsr WasteScanlines

    ; ------------------------------------------------------------
    ; Lower bars
    ; ------------------------------------------------------------

PaLowerBars SUBROUTINE
    lda #%00110000
    sta PF0
    ; Position objects and init colors
    lda pa_posHi+PA_NUM_BARS-1
    jsr PaPosObjects
    sta pa_curCol1
    stx pa_curCol2

    ldx #(PA_NUM_BARS - 1)
.loop:
    ldy BarHeights,x
.loop2:
    lda pa_curCol1
    adc BarGradient,y
    adc BarHeights,x
    pha
    lda pa_curCol2
    sta WSYNC
    sta HMOVE
    adc BarGradient,y
    adc BarHeights,x
    sta COLUBK
    pla
    sta COLUP0
    sta COLUP1
    sta HMCLR
    dey
    bpl .loop2
    lda pa_hmValues,x
    ldy pa_switchColFlags,x
    jsr PaHmCheckColSwitch
    dex
    bpl .loop

    sta WSYNC
    sta WSYNC
    jmp CleanUpGfxWithBg

; TIATracker music player
; Copyright 2016 Andre "Kylearan" Wichmann
; Website: https://bitbucket.org/kylearan/tiatracker
; Email: andre.wichmann@gmx.de
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;   http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

; Song author:
; Song name:

; =====================================================================
; TIATracker Player
; =====================================================================
tt_PlayerStart:

; PLANNED PLAYER VARIANTS:
; - RAM, speed, player ROM: c0/c1 patterns have same length
; - RAM: Pack 2 values (out of cur_pat_index, cur_note_index, envelope_index)
;       into one and use lsr/asl to unpack them, allowing only ranges of
;       16/16 or 32/8 for them, depending on number of patterns, max
;       pattern size and max ADSR size
; - ROM: Check if tt_SequenceTable can hold ptrs directly without indexing
;       tt_PatternPtrLo/Hi. Can be smaller if not many patterns get repeated
;       (saves table and decode routine)
; - Speed: Inline tt_CalcInsIndex
; - Speed: Store ptr to current note in RAM instead of reconstructing it?
;       Might also save the need for cur_note_index


EnsureNormalMusic   SUBROUTINE
    ; Test if correct music is played
    lda tt_cur_pat_index_c0
    bne .correctMusic
    sta tt_cur_note_index_c0
    sta tt_cur_note_index_c1
    sta tt_timer
    lda #MUSIC_START_0
    sta tt_cur_pat_index_c0
    lda #MUSIC_START_1
    sta tt_cur_pat_index_c1
.correctMusic:
    ; Fall through to player

; ---------------------------------------------------------------------
; Music player entry. Call once per frame.
; ---------------------------------------------------------------------
tt_Player SUBROUTINE
        ; ==================== Sequencer ====================
        ; Decrease speed timer
        dec tt_timer
        bpl .noNewNote

        ; Timer ran out: Do sequencer
        ; Advance to next note
        ldx #1                          ; 2 channels
.advanceLoop:
        ; construct ptr to pattern
.constructPatPtr:
        ldy tt_cur_pat_index_c0,x       ; get current pattern (index into tt_SequenceTable)
        lda tt_SequenceTable,y
        bpl .noPatternGoto
        and #%01111111                  ; mask out goto bit to get pattern number
        sta tt_cur_pat_index_c0,x       ; store goto'ed pattern index
        bpl .constructPatPtr            ; unconditional
.noPatternGoto:
        clc
        adc tt_cur_note_index_c0,x
        tay
        lda tt_pattern0,y
        ; pre-process new note
        ; 7..5: instrument (1..7), 4..0 (0..31): frequency
        ; 0/0: End of pattern
        bne .noEndOfPattern
        ; End of pattern: Advance to next pattern
        sta tt_cur_note_index_c0,x      ; a is 0
        inc tt_cur_pat_index_c0,x
        bne .constructPatPtr            ; unconditional
.noEndOfPattern:


        ; Parse new note from pattern
        cmp #TT_INS_PAUSE
        bcc .finishedNewNote
        bne .newNote

        ; --- pause ---
.pause:
        ; Get release index for current instrument. Since a pause can
        ; only follow an instrument, we don't need to handle percussion
        ; or commands.
        lda tt_cur_ins_c0,x
        jsr tt_CalcInsIndex
        lda tt_InsReleaseIndexes-1,y    ; -1 b/c instruments start at #1
        ; Put it into release. Skip junk byte so index no longer indicates
        ; sustain phase.
        clc
        adc #1
        bcc .storeADIndex               ; unconditional

        ; --- start instrument or percussion ---
.newNote:
        sta tt_cur_ins_c0,x             ; set new instrument
        ; Instrument or percussion?
        cmp #TT_FREQ_MASK+1
        bcs .startInstrument

        ; --- start percussion ---
        lda #1
        bne .storeADIndex               ; unconditional, since index values are >0

        ; --- start instrument ---
.startInstrument:
        ; Put note into attack/decay
        jsr tt_CalcInsIndex
        lda tt_InsADIndexes-1,y         ; -1 because instruments start at #1
.storeADIndex:
        sta tt_envelope_index_c0,x

        ; --- Finished parsing new note ---
.finishedNewNote:
        ; increase note index into pattern
        inc tt_cur_note_index_c0,x
        ; loop over channels
.sequencerNextChannel:
        dex
        bpl .advanceLoop

        ; Reset timer value
        lda #(MUSIC_RELIEF_SPEED - 1)
        ldx tt_cur_pat_index_c0
        beq .staticTempo
        lda #(MUSIC_SPEED - 1)
        sec
        sbc aggression
        sbc aggression
.staticTempo:
        sta tt_timer

        ; No new note to process
.noNewNote:

        ; ==================== Update registers ====================
        ldx #1                          ; 2 channels
.updateLoop:
        ; Percussion or melodic instrument?
        lda tt_cur_ins_c0,x
    IF TT_STARTS_WITH_NOTES = 0
        ; This branch can be removed if track starts with a note in each channel
        beq .afterAudioUpdate
    ENDIF
        cmp #TT_FREQ_MASK+1
        bcs .instrument                 ; Melodic instrument

        ; --- Percussion: Get envelope index ---
        ldy tt_envelope_index_c0,x
        ; Set AUDC and AUDV value from envelope
        lda tt_PercCtrlVolTable-1,y     ; -1 because values are stored +1
        beq .endOfPercussion            ; 0 means end of percussion data
        inc tt_envelope_index_c0,x      ; if end not reached: advance index
.endOfPercussion:
        sta AUDV0,x
        lsr
        lsr
        lsr
        lsr
        sta AUDC0,x
        ; Set AUDF
        lda tt_PercFreqTable-1,y        ; -1 because values are stored +1
        ; Bit 7 (overlay) might be set, but is unused in AUDF
        sta AUDF0,x
        jmp .afterAudioUpdate


; ---------------------------------------------------------------------
; Helper subroutine to minimize ROM footprint.
; Interleaved here so player routine can be inlined.
; ---------------------------------------------------------------------
tt_CalcInsIndex:
        ; move upper 3 bits to lower 3
        lsr
        lsr
        lsr
        lsr
        lsr
        tay
tt_Bit6Set:     ; This opcode has bit #6 set, for use with bit instruction
        rts


.instrument:
        ; --- Melodic instrument ---
        ; Compute index into ADSR indexes and master Ctrl tables
        jsr tt_CalcInsIndex
        ; Set AUDC with master value for this instrument, while we are at it
        lda tt_InsCtrlTable-1,y ; -1 because instruments start with #1
        sta AUDC0,x
        ; advance ADSR counter and compare to end of Sustain
        lda tt_envelope_index_c0,x
        cmp tt_InsReleaseIndexes-1,y    ; -1 because instruments start with #1
        bne .noEndOfSustain
        ; End of sustain: Go back to start of sustain
        lda tt_InsSustainIndexes-1,y    ; -1 because instruments start with #1
        ; Check for distortion
        ldy tt_cur_pat_index_c0
        bne .noDistortion
        ldy rl_mode
        bmi .noDistortion
        sty AUDC0,x
.noDistortion:
.noEndOfSustain:
        tay
        ; Set volume from envelope
        lda tt_InsFreqVolTable,y
        beq .endOfEnvelope              ; 0 means end of release has been reached:
        iny                             ; advance index otherwise
.endOfEnvelope:
        sty tt_envelope_index_c0,x
        sta AUDV0,x
        ; Now adjust frequency with ADSR value from envelope
        lsr
        lsr
        lsr
        lsr
        clc
        adc tt_cur_ins_c0,x
        sec
        sbc #8
        sta AUDF0,x

.afterAudioUpdate:
        ; loop over channels
        dex
        bpl .updateLoop

        echo "[M]", EnsureNormalMusic, "to", *, ": EnsureNormalMusic"

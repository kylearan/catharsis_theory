; =====================================================================
; Flags
; =====================================================================

MUSIC_RELIEF_SPEED      = 7

    IF MUSIC = 1
TT_STARTS_WITH_NOTES    = 1
; Music start indexes
MUSIC_START_0           = 2
MUSIC_START_1           = 7
MUSIC_RELIEF_0          = 0
MUSIC_RELIEF_1          = 5
MUSIC_SPEED             = 10
    ENDIF

    IF MUSIC = 2
TT_STARTS_WITH_NOTES    = 1
; Music start indexes
MUSIC_START_0           = 2
MUSIC_START_1           = 9
MUSIC_RELIEF_0          = 0
MUSIC_RELIEF_1          = 7
MUSIC_SPEED             = 12
    ENDIF

    IF MUSIC = 3
TT_STARTS_WITH_NOTES    = 1
; Music start indexes
MUSIC_START_0           = 2
MUSIC_START_1           = 13
MUSIC_RELIEF_0          = 0
MUSIC_RELIEF_1          = 11
MUSIC_SPEED             = 15
    ENDIF

tt_timer                ds 1    ; current music timer value
tt_cur_pat_index_c0     ds 1    ; current pattern index into tt_SequenceTable
tt_cur_pat_index_c1     ds 1
tt_cur_note_index_c0    ds 1    ; note index into current pattern
tt_cur_note_index_c1    ds 1
tt_envelope_index_c0    ds 1    ; index into ADSR envelope
tt_envelope_index_c1    ds 1
tt_cur_ins_c0           ds 1    ; current instrument
tt_cur_ins_c1           ds 1

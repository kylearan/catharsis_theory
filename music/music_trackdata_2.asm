; TIATracker music player
; Copyright 2016 Andre "Kylearan" Wichmann
; Website: https://bitbucket.org/kylearan/tiatracker
; Email: andre.wichmann@gmx.de
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;   http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

; Song author:
; Song name:

; =====================================================================
; TIATracker melodic and percussion instruments, patterns and sequencer
; data.
; =====================================================================
tt_TrackDataStart:

; =====================================================================
; Melodic instrument definitions (up to 7). tt_envelope_index_c0/1 hold
; the index values into these tables for the current instruments played
; in channel 0 and 1.
;
; Each instrument is defined by:
; - tt_InsCtrlTable: the AUDC value
; - tt_InsADIndexes: the index of the start of the ADSR envelope as
;       defined in tt_InsFreqVolTable
; - tt_InsSustainIndexes: the index of the start of the Sustain phase
;       of the envelope
; - tt_InsReleaseIndexes: the index of the start of the Release phase
; - tt_InsFreqVolTable: The AUDF frequency and AUDV volume values of
;       the envelope
; =====================================================================

; Instrument master CTRL values
tt_InsCtrlTable:
        dc.b $04, $0c, $06



; Instrument Sustain start indexes into ADSR tables
tt_InsSustainIndexes:
        dc.b $02, $02, $06


; tt_InsReleaseIndexes is now in tunnel.asm

; tt_InsFreqVolTable + tt_InsADIndexes is now in parallax.asm


; =====================================================================
; Percussion instrument definitions (up to 15)
;
; Each percussion instrument is defined by:
; - tt_PercIndexes: The index of the first percussion frame as defined
;       in tt_PercFreqTable and tt_PercCtrlVolTable
; - tt_PercFreqTable: The AUDF frequency value
; - tt_PercCtrlVolTable: The AUDV volume and AUDC values
; =====================================================================

; The AUDCx and AUDVx volume values for the percussion instruments.
; - Bits 7..4: AUDC value
; - Bits 3..0: AUDV value
; 0 means end of percussion data.
tt_PercCtrlVolTable:
; 0: Drum
        dc.b $8f, $8d, $8b, $89, $87
        ; Overlap
        ;dc.b $00
; The AUDF frequency values for the percussion instruments.
; If the second to last value is negative (>=128), it means it's an
; "overlay" percussion, i.e. the player fetches the next instrument note
; immediately and starts it in the sustain phase next frame. (Needs
; TT_USE_OVERLAY)
tt_PercFreqTable:
; 0: Drum
        dc.b $00, $03, $06, $09, $0c, $00



; =====================================================================
; Track definition
; The track is defined by:
; - tt_PatternX (X=0, 1, ...): Pattern definitions
; - tt_PatternPtrLo/Hi: Pointers to the tt_PatternX tables, serving
;       as index values
; - tt_SequenceTable: The order in which the patterns should be played,
;       i.e. indexes into tt_PatternPtrLo/Hi. Contains the sequences
;       for all channels and sub-tracks. The variables
;       tt_cur_pat_index_c0/1 hold an index into tt_SequenceTable for
;       each channel.
;
; So tt_SequenceTable holds indexes into tt_PatternPtrLo/Hi, which
; in turn point to pattern definitions (tt_PatternX) in which the notes
; to play are specified.
; =====================================================================

; ---------------------------------------------------------------------
; Pattern definitions, one table per pattern. tt_cur_note_index_c0/1
; hold the index values into these tables for the current pattern
; played in channel 0 and 1.
;
; A pattern is a sequence of notes (one byte per note) ending with a 0.
; A note can be either:
; - Pause: Put melodic instrument into release. Must only follow a
;       melodic instrument.
; - Hold: Continue to play last note (or silence). Default "empty" note.
; - Slide (needs TT_USE_SLIDE): Adjust frequency of last melodic note
;       by -7..+7 and keep playing it
; - Play new note with melodic instrument
; - Play new note with percussion instrument
; - End of pattern
;
; A note is defined by:
; - Bits 7..5: 1-7 means play melodic instrument 1-7 with a new note
;       and frequency in bits 4..0. If bits 7..5 are 0, bits 4..0 are
;       defined as:
;       - 0: End of pattern
;       - [1..15]: Slide -7..+7 (needs TT_USE_SLIDE)
;       - 8: Hold
;       - 16: Pause
;       - [17..31]: Play percussion instrument 1..15
;
; The tracker must ensure that a pause only follows a melodic
; instrument or a hold/slide.
; ---------------------------------------------------------------------
TT_FREQ_MASK    = %00011111
TT_INS_HOLD     = 8
TT_INS_PAUSE    = 16
TT_FIRST_PERC   = 17

; Intro left
TT_PAT_0        = 0
tt_pattern0:
        dc.b $33, $31, $2e, $2b, $08, $33, $31, $2e
        dc.b $33, $08, $33, $31, $08, $37, $33, $08
        dc.b $00

; L2
TT_PAT_1        = * - tt_pattern0
tt_pattern1:
        dc.b $69, $4c, $11, $68, $6b, $4f, $11, $6b
        dc.b $69, $4c, $11, $68, $67, $4f, $11, $11
        dc.b $00

; Intro right
TT_PAT_2        = * - tt_pattern0
tt_pattern2:
        dc.b $08, $33, $31, $2e, $2b, $08, $33, $31
        dc.b $2e, $31, $08, $27, $28, $29, $28, $37
        dc.b $00

; R2
TT_PAT_3        = * - tt_pattern0
tt_pattern3:
        dc.b $65, $67, $65, $64, $4c, $62, $4c, $4f
        dc.b $61, $08, $64, $63, $65, $08, $67, $65
        dc.b $6e, $08, $08, $08, $71, $08, $08, $08
        dc.b $6e, $08, $08, $08, $71, $08, $68, $08
        dc.b $65, $67, $65, $64, $4c, $63, $62, $61
        dc.b $63, $60, $64, $63, $65, $08, $67, $4c
        dc.b $6e, $08, $08, $08, $6b, $08, $71, $08
        dc.b $6e, $08, $08, $08, $6b, $08, $71, $08
        ; Overlap
        ;dc.b $00
; ---------------------------------------------------------------------
; Pattern sequence table. Each byte is an index into the
; tt_PatternPtrLo/Hi tables where the pointers to the pattern
; definitions can be found. When a pattern has been played completely,
; the next byte from this table is used to get the address of the next
; pattern to play. tt_cur_pat_index_c0/1 hold the current index values
; into this table for channels 0 and 1.
; If TT_USE_GOTO is used, a value >=128 denotes a goto to the pattern
; number encoded in bits 6..0 (i.e. value AND %01111111).
; ---------------------------------------------------------------------
tt_SequenceTable:
        ; ---------- Channel 0 ----------
        ;dc.b $00, $80, $01, $01, $01, $01, $82
        dc.b TT_PAT_0, 0|128, TT_PAT_1, TT_PAT_1, TT_PAT_1, TT_PAT_1, 2|128

        ; ---------- Channel 1 ----------
        ;dc.b $02, $87, $03, $89
        dc.b TT_PAT_2, 7|128, TT_PAT_3, 9|128

        echo "[M]", tt_TrackDataStart, "to", *, ": tt_TrackDataStart"

    echo ""
    echo "------------------------------------------------------------"
    echo "WHITENOISE:"

; TODO:
; - check if Cosmic Ark effect works on real hardware. If not: Remove
;   "jsr wn_StaHMx" again and do a SLEEP 5; sta ... instead.


; =====================================================================
; VBlank
; =====================================================================

; ---------------------------------------------------------------------
; VBlank main
; ---------------------------------------------------------------------
WhiteNoiseVBlank    SUBROUTINE
    ; Position objects.
    ; No WSYNC needed: The distance is important, not the absolute
    ; position.
    ; numFrame is in a already
    tay
    ldx #4
.loop:
    dey
    bne .loop
.loop2:
    sta RESP0,x
    dex
    bpl .loop2
    ; x=255
    stx AUDV0
    stx AUDF0

    ; Set colors and sound
    lda #$02
    sta COLUBK
    lda #$0e
    sta COLUP0
    sta COLUP1
    lda #$08
    sta COLUPF
    sta AUDC0

    IF START_PART = 0
        ; Init objects
        lda #%00110111      ; Missile size 4, Player size 4
        sta NUSIZ0
        sta NUSIZ1
        sta ENAM0
        sta ENAM1
        sta ENABL
        lda #%00100000      ; Ball size 2. REMOVE FOR SIZE?
        sta CTRLPF
        sta GRP0
        sta GRP1
    ENDIF

    ; Fall through to InitCosmicArk
InitCosmicArk   SUBROUTINE
    lda #$70
    jsr wn_StaHMx
    lda #$c0
    sta WSYNC
    sta HMOVE
    jsr wn_StaHMx       ; need jsr instead of jmp for timing
    rts



; =====================================================================
; Kernel
; =====================================================================

WhiteNoiseKernel    SUBROUTINE
    ; numFrame is in a already
    ldy #3
    ldx #$7c            ; Height of white noise kernel *3
.loop:
    cmp #0
    beq .doEor
    asl
    beq .noEor
    bcc .noEor
.doEor
    eor #$1d
.noEor
    sta PF0
    sta PF1
    sta PF2
    dex
    bne .loop
    dey
    bne .loop

    ; a is 0 already
    sta AUDC0
    sta AUDC1
    jmp CleanUpGfxWithBg

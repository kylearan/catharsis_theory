#!/usr/bin/python3

import sys
import math

for i in range(0, 64):
	s = round(15 + 15*math.cos((i*(2*math.pi)/64) + math.pi/2))
	if i%8 == 0:
		print("\n    dc.b ", end="")
	print(s, end="")
	if i%8 != 7:
		print(", ", end="")
print("")

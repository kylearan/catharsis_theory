from PIL import Image
import sys

BLOCK_SET = (0, 0, 0)
BLOCK_CLEAR = (0xff, 0xff, 0xff)


def pixel(x, y):
    if rgb.getpixel((x, y)) == BLOCK_SET:
        return 1
    if rgb.getpixel((x, y)) == BLOCK_CLEAR:
        return 0
    print("Error at", x, y, ":", rgb.getpixel((x, y)))
    sys.exit(1)


def convert_byte(x, y):
    res = "%"
    for i in range(0, 8):
        res += ("%s" % pixel(x + i, y))
    return res


def to_dcb(r):
    for i in range(0, len(r)):
        if i%8 == 0:
            print("\n    dc.b ", end="")
        print(r[i], end="")
        if i%8 != 7 and i != len(r) - 1:
            print(", ", end="")
    print("\n")


infile = sys.argv[1]
label = sys.argv[2]

img = Image.open(infile)
rgb = img.convert('RGB')
if rgb.size[0] != 16:
    print("Width error:", rgb.size[0])
    sys.exit(1)
gfxheight = rgb.size[1]

row1 = []
row2 = []
for y in range (0, gfxheight):
    row1.append(convert_byte(0, gfxheight - y - 1))
    row2.append(convert_byte(8, gfxheight - y - 1))
print("%s_1:" % label, end="")
to_dcb(row1)
print("%s_2:" % label, end="")
to_dcb(row2)

#!/usr/bin/python3

SCR_SIZE = 32
TEX_SIZE = 16
DIST_SCALE = 10
ANGLE_SCALE = 2.15

PRECISION_FAC = 2	# 1, 2, 4, 8, 16; max dist/angle must be <256
ARRAY_FAC = 8		# factor for pre-calc for dist use as array index
NUM_STEPS = 32		# should be TEX_SIZE*PRECISION_FAC?


import sys
import math
import os
import time

from PIL import Image


distances = [[0 for x in range(SCR_SIZE)] for x in range(SCR_SIZE)]
angles = [[0 for x in range(SCR_SIZE)] for x in range(SCR_SIZE)]

tex_1 = Image.open("texture_1.png")
tex_2 = Image.open("texture_3.png")


def gen_data():
	global tex_1, tex_2, angles, distances

	# create angle table
	min = 9999
	max = -9999
	for y in range(SCR_SIZE):
		for x in range(SCR_SIZE):
			angle = int(PRECISION_FAC * ANGLE_SCALE*(SCR_SIZE/2 + 0.5*SCR_SIZE*math.atan2(y - SCR_SIZE/2, x - SCR_SIZE/2)/math.pi) + 0.5) % (TEX_SIZE * PRECISION_FAC)
			if angle < min:
				min = angle
			if angle > max:
				max = angle
			if angle < 0:
				angle += TEX_SIZE*PRECISION_FAC
			angles[x][y] = angle
	print("Angle min: %d, Max: %d" % (min, max))

	# create distance table
	min = 9999
	max = -9999
	for y in range(SCR_SIZE):
		for x in range(SCR_SIZE):
			rel_x = int(x - SCR_SIZE/2)
			if rel_x is 0:
				rel_x = 0.00000001
			rel_y = int(SCR_SIZE/2 - y)
			if rel_y is 0:
				rel_y = 0.00000001
			# *16 for pre-calcing array fetch (y*16+x)
			distances[x][y] = ARRAY_FAC*(int(PRECISION_FAC * DIST_SCALE*TEX_SIZE/math.sqrt(rel_x*rel_x + rel_y*rel_y) + 0.5) % (TEX_SIZE * PRECISION_FAC))
			if distances[x][y] < min:
				min = distances[x][y]
			if distances[x][y] > max:
				max = distances[x][y]
	print("Distance min: %d, Max: %d" % (min, max))

	# Add textures to angle table
	for y in range(16):
		for x in range(16):
			if tex_1.getpixel((x, y)) == (0, 0, 0):
				angles[x][y] |= 128
			if tex_2.getpixel((x, y)) == (0, 0, 0):
				angles[x][y] |= 64


def print_tables():
	global angles, distances

	# angles
	print("TunnelAngles:", end="")
	for y in range(int(SCR_SIZE/2)):
		print("\n\tdc.b ", end="")
		for x in range(int(SCR_SIZE/2)):
			print("$%02x" % (angles[x][y]), end="")
			if x < int(SCR_SIZE/2 - 1):
				print(", ", end="")
	print("\n")

	# distances
	print("TunnelDistances:", end="")
	for y in range(int(SCR_SIZE/2)):
		print("\n\tdc.b ", end="")
		for x in range(int(SCR_SIZE/2)):
			print("$%02x" % (distances[x][y]), end="")
			if x < int(SCR_SIZE/2 - 1):
				print(", ", end="")
	print("\n")



if __name__ == "__main__":
	gen_data()
	print_tables()

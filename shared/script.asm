; =====================================================================
; Constants
; =====================================================================

; Index constants for the different parts
PART_NOTHING    = 0
PART_LIGHTNING  = 1
PART_TUNNEL     = 2
PART_TRIANGLE   = 3
PART_PARALLAX   = 4
PART_GAME       = 5
PART_RELIEF     = 6
PART_DESCENT    = 7
PART_REALITY    = 8
PART_WHITENOISE = 9

DUR_PART00      = 511   ; Nothing

DUR_PART01      = 200   ; Lightning
DUR_PART02      = 385   ; Tunnel
DUR_PART03      = 195   ; Lightning
DUR_PART04      = 385   ; Triangle
DUR_PART05      = 195   ; Lightning
DUR_PART06      = 385   ; Parallax
DUR_PART07      = 190   ; Lightning
DUR_PART08      = 255   ; Game
DUR_PART09      = 511   ; Relief

DUR_PART10      = 175   ; Lightning
DUR_PART11      = 320   ; Tunnel
DUR_PART12      = 165   ; Lightning
DUR_PART13      = 320   ; Triangle
DUR_PART14      = 165   ; Lightning
DUR_PART15      = 320   ; Parallax
DUR_PART16      = 160   ; Lightning
DUR_PART17      = 255   ; Game
DUR_PART18      = 511   ; Relief

DUR_PART19      = 140   ; Lightning
DUR_PART20      = 255   ; Tunnel
DUR_PART21      = 140   ; Lightning
DUR_PART22      = 255   ; Triangle
DUR_PART23      = 140   ; Lightning
DUR_PART24      = 255   ; Parallax
DUR_PART25      = 130   ; Lightning
DUR_PART26      = 190   ; Game
DUR_PART27      = 450   ; Relief

DUR_PART28      = 511   ; Descent
DUR_PART29      = 290   ; Reality
DUR_PART30      = 500   ; Whitenoise
DUR_PART31      = 511   ; Nothing


; =====================================================================
; Data
; =====================================================================

; Pointers VBlank and Kernel of all parts
; Overscan is only needed for Tunnel and Descent and will be manually jumped to
VBlankLo:
    dc.b <(Return-1), <(LightningVBlank-1), <(TunnelVBlank-1), <(TriangleVBlank-1)
    dc.b <(ParallaxVBlank-1), <(GameVBlank-1), <(ReliefVBlank-1), <(DescentVBlank-1)
    dc.b <(Return-1), <(WhiteNoiseVBlank-1)
VBlankHi:
    dc.b >(Return-1), >(LightningVBlank-1), >(TunnelVBlank-1), >(TriangleVBlank-1)
    dc.b >(ParallaxVBlank-1), >(GameVBlank-1), >(ReliefVBlank-1), >(DescentVBlank-1)
    dc.b >(Return-1), >(WhiteNoiseVBlank-1)

KernelLo:
    dc.b <(Return-1), <(LightningKernel-1), <(TunnelKernel-1), <(TriangleKernel-1)
    dc.b <(ParallaxKernel-1), <(GameKernel-1), <(ReliefKernel-1), <(DescentKernel-1)
    dc.b <(RealityKernel-1), <(WhiteNoiseKernel-1)
KernelHi:
    dc.b >(Return-1), >(LightningKernel-1), >(TunnelKernel-1), >(TriangleKernel-1)
    dc.b >(ParallaxKernel-1), >(GameKernel-1), >(ReliefKernel-1), >(DescentKernel-1)
    dc.b >(RealityKernel-1), >(WhiteNoiseKernel-1)

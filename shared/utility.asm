    echo ""
    echo "------------------------------------------------------------"
    echo "DESCENT:"

; ---------------------------------------------------------------------
; Waste x scanlines
; ---------------------------------------------------------------------
WasteScanlines  SUBROUTINE
.loop:
    sta WSYNC
    dex
    bne .loop
    rts


; ---------------------------------------------------------------------
; Jitter on aggression
; IN:
;   a   numFrame
; OUT:
;   a   numFrames-1 wasted
; ---------------------------------------------------------------------
DoJitter    SUBROUTINE
    ; Check for aggression jitter
    ldx aggression
    cpx #2
    bne .noJitter
DoJitterAllAggression:
    lsr
    jsr Rnd
    and #3
    tax
    inx
    jmp WasteScanlines
.noJitter:
    lda #0
    rts


; ---------------------------------------------------------------------
; Get flash color
; ---------------------------------------------------------------------

GetFlashCol SUBROUTINE
    lda numFrame
    jsr Rnd
    and #15
    adc #$20
    rts


; ---------------------------------------------------------------------
; Init Cosmic Ark effect for all objects
; ---------------------------------------------------------------------

; Helper subroutine to store a in all HM regs, for size.
wn_StaHMx   SUBROUTINE
    sta HMM0
    sta HMM1
    sta HMBL
    sta HMP0
    sta HMP1
    rts


; ---------------------------------------------------------------------
; Fine-position object. Updates HMx, but does no HMOVE
; IN:
;   a   xPos, 1..160
;   x   object number, 0..4
; OUT:
;   x   object number
;   c   clear
; ---------------------------------------------------------------------
FinePosObject   SUBROUTINE
    sec
    sta WSYNC
FinePosDivideLoop:
    sbc #15
    bcs FinePosDivideLoop

    echo "[M]", FinePosDivideLoop, "to", *, ": FinePosDivideLoop"

    eor #7
    asl
    asl
    asl
    asl
    sta HMP0,x
    sta.w RESP0,x
    ; Prepare for next FinePosObject call, to save space
    inx
    ; we enter via brk, so rti instead of rts
    rti


; ---------------------------------------------------------------------
; Generate pseudo-rng.
; IN:
;   a   seed with flags from lda
; OUT:
;   a   new seed
; ---------------------------------------------------------------------
Rnd SUBROUTINE
    beq .doEor
    asl
    beq .noEor
    bcc .noEor
.doEor
    eor #$1d
.noEor
Return:
    rts
